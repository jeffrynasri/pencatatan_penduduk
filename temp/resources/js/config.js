$('#dataBidang').DataTable({
	"language": {
		"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Indonesian.json"
	},
	"columnDefs": [
		{ "visible": false, "targets": 0 }
	],
	"order": [[1, 'asc' ]],
	"displayLength": 10,
	"drawCallback": function (settings) {
		var api = this.api();
		var rows = api.rows({page:'current'}).nodes();
		var last = null;
 
 		api.column(0, {page:'current'}).data().each(function (group, i){
			if (last !== group){
				$(rows).eq(i).before(
					'<tr class="group-program"><td></td><td colspan="5"><strong>'+group+'<strong></td></tr>'
				);
				last = group;
			}
		});
 	}
});

var daftarKegiatan = $('#daftarKegiatan').DataTable({
	"language": {
		"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Indonesian.json"
	}
});

var dataProgram = $('#dataProgram').DataTable({
	"language": {
		"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Indonesian.json"
	}
});

var dataSKPD = $('#dataSKPD').DataTable({
	"language": {
		"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Indonesian.json"
	}
});

$('#daftarKegiatan tbody').on('click', 'tr', function() {
	var data                 = daftarKegiatan.row(this).data();
	var url                  = window.document.location.pathname.replace('program', 'kegiatan');
	url                      = url.split('/');
	url[url.length-1]        = data[0];
	url                      = url.join('/');
	window.document.location = url;
});

var dataKegiatan = $('#dataKegiatan').DataTable({
	"language": {
		"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Indonesian.json"
	},
	"columnDefs": [
		{ "visible": false, "targets": 0 },
		{ "visible": false, "targets": 1 },
		{ "visible": false, "targets": 5 }
	],
	"order": [[2, 'asc' ]],
	"displayLength": 50,
	"drawCallback": function (settings) {
		var api = this.api();
		var rows = api.rows({page:'current'}).nodes();
		var last = null;
 
 		api.column(1, {page:'current'}).data().each(function (namaProgram, i){
			if (last !== namaProgram){
				var rowData = api.row(i).data();
				$(rows).eq(i).before(
					'<tr class="success"><td><strong>'+rowData[0]+'</strong></td><td><strong>'+namaProgram+'<strong></td><td><strong>'+rowData[5]+'</strong></td></tr>'
				);
				last = namaProgram;
			}
		});
 	}
});

/*$('#listPD').append('<option value="">-- Pilih Perangkat Daerah --</option>');
$.getJSON('<?php echo base_url('assets/acuan/daftar_skpd.json') ?>', function(data){
	$.each(data, function (i, val) {
		$('#listPD').append(
			'<option value="' + val.kode_pd + '">' + val.nama_pd + '</option>'
		);
	});
});*/