function checkNotNull(not_null_dom)
{
  //cek null input
  if (not_null_dom != null && not_null_dom.length > 0) {
    for (var i = 0; i < not_null_dom.length; i++) {
      if ($('[name = "' + not_null_dom[i] + '"]').val().trim() == "") {
        $('[name = "' + not_null_dom[i] + '"]').focus();
        Swal.fire("Isian Data Belum Lengkap", '', "error");
        return false;
      }
    }
  }

  return true;
}

function checkSameValue(dom1, dom2)
{
  //cek value sama
  if ($('[name = "' + dom1 + '"]').val() != $('[name = "' + dom2 + '"]').val()) {
    $('[name = "' + dom2 + '"]').focus();
    Swal.fire("Isian Data Tidak Sama", '', "error");
    return false;
  }
  return true;
}

function saveData(not_null_dom = null, url_add, url_edit, form_id)
{
  //cek null input
  if (not_null_dom != null && not_null_dom.length > 0) {
    for (var i = 0; i < not_null_dom.length; i++) {
      if ($('[name = "' + not_null_dom[i] + '"]').val().trim() == "") {
        $('[name = "' + not_null_dom[i] + '"]').focus();
        Swal.fire("Isian Data Belum Lengkap", '', "error");
        return;
      }
    }
  }

  //cek save_mode
  if (save_mode == "add") {
    createData(url_add, form_id);
  }
  else {
    updateData(url_edit, form_id);
  }
}

function createData(url_add, form_id)
{
  $.ajax({
    url : url_add,
    type: "POST",
    data: $('#' + form_id).serialize(),
    dataType: "JSON",
    beforeSend: function()
    {
      $('.div-loading-overlay').show();
    },
    success: function(data)
    {
      if (data.success)
      {
        reset_form();
        read_data();
        Swal.fire({
          type: 'success',
          title: 'Data Tersimpan',
          showConfirmButton: false,
          timer: 500
        });
      }
      else {
        Swal.fire("Operasi Data Gagal", '', "error");
      }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      Swal.fire("Error : Operasi Data Gagal", errorThrown, "error");
    },
    complete: function()
    {
      $('.div-loading-overlay').hide();
    }
  });
}

function updateData(url_edit, form_id)
{
  $.ajax({
    url : url_edit,
    type: "POST",
    data: $('#' + form_id).serialize(),
    dataType: "JSON",
    beforeSend: function()
    {
      $('.div-loading-overlay').show();
    },
    success: function(data)
    {
      if (data.success)
      {
        Swal.fire({
          type: 'success',
          title: 'Data Tersimpan',
          showConfirmButton: false,
          onClose: adjustDT,
          timer: 500
        });
        read_data();
        // reset_form();
        $('#div_modal').modal('toggle');
      }
      else {
        Swal.fire("Operasi Data Gagal", '', "error");
      }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      Swal.fire("Error : Operasi Data Gagal", errorThrown, "error");
    },
    complete: function()
    {
      $('.div-loading-overlay').hide();
    }
  });
}

function deleteData(url_delete, param_value)
{
  url = url_delete;

  Swal.fire({
    title: "Yakin Hapus Data ?",
    text: "",
    type: 'question',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Batal'
  }).then((result) => {
    if (result.value) {
      //call ajax
      $.ajax({
          url : url,
          type: "POST",
          data: param_value,
          dataType: "JSON",
          beforeSend: function()
          {
              $('.div-loading-overlay').show();
          },
          success: function(data)
          {
            if (data.success) {
              oTable.ajax.reload(null, false);
              Swal.fire({
                type: 'success',
                title: 'Data Sudah Terhapus',
                showConfirmButton: false,
                onClose: adjustDT,
                timer: 500
              });
            }
            else {
              Swal.fire("Hapus Data Gagal", "", "error");
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            Swal.fire("Error : Operasi Data Gagal", "", "error");
          },
          complete: function()
          {
            $('.div-loading-overlay').hide();
          }
      });
    }
  });
}

function inisialisasiDtCs(table_id, url_data, obj_post_value, arr_data_col, arr_col_defs, footer_callback = null)
{
  // data_col = arr_data_col.map(function(d) {
  //   return { data: d };
  // });


  // data_col = [];
  // if (arr_data_col.length > 0) {
  //   for (var i = 0; i < arr_data_col.length; i++) {
  //     data_col.push({ "data": arr_data_col[i] });
  //   }
  // }
  // else {
  //   //do nothing
  // }

  //object buat option
  //todo
  //
  // dt_option = {};
  // dt_option['processing'] = true;
  // dt_option['serverSide'] = false;



  $('#' + table_id).show();

  oTable = $('#' + table_id).DataTable({
    processing: true,
    serverSide: false,
    order: [], //Initial no order.
    scrollY: '555px',
    scrollCollapse: true,
    paging: true,
    info: true,
    autoWidth: true,
    scrollX: true,
    ordering: true,
    ajax: {
      url: url_data,
      type: "POST",
      data: function(d) {
        if (Object.keys(obj_post_value).length > 0 && obj_post_value != null) {
          for (var tmp in obj_post_value) {
            //d[tmp] = $('[name = "' + tmp + '"]').val();
            d[tmp] = $('[name = "' + tmp + '"]').val();
          }
        }
        else {
          //do nothing
        }
      }
    },
    columns: arr_data_col.map(function(d) {
      return { data: d };
    }),
    columnDefs: arr_col_defs.map(function(d) {
      return d;
    }),
    footerCallback: function ( row, data, start, end, display ) {
      if (footer_callback === null) {
        return;
      }
      else {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column(footer_callback.col_total)
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Update footer
        $( api.column(footer_callback.col_display).footer() ).html(
            //'$'+pageTotal +' ( $'+ total +' total)'
            total
        );
      }


    },
    // footerCallback: function() {
    //   if (footer_callback == null) {
    //     console.log('asdasd');
    //     return null;
    //   }
    //   else if (footer_callback == 'db') {
    //     // console.log('DBBB');
    //     // return null;
    //     return {
    //       function ( row, data, start, end, display ) {
    //         var api = this.api(), data;
    //
    //         // Remove the formatting to get integer data for summation
    //         var intVal = function ( i ) {
    //             return typeof i === 'string' ?
    //                 i.replace(/[\$,]/g, '')*1 :
    //                 typeof i === 'number' ?
    //                     i : 0;
    //         };
    //
    //         // Total over all pages
    //         total = api
    //             .column( 7 )
    //             .data()
    //             .reduce( function (a, b) {
    //                 return intVal(a) + intVal(b);
    //             }, 0 );
    //
    //         // Update footer
    //         $( api.column( 6 ).footer() ).html(
    //             //'$'+pageTotal +' ( $'+ total +' total)'
    //             'TESSSSS'
    //         );
    //
    //       }
    //     }
    //   }
    // },
  });

  oTable.columns.adjust();
}
