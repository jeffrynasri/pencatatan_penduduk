<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    <!-- Sidebar user panel -->
           
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">NAVIGASI UTAMA</li>

          
            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Buku Tamu</span>
                </a>
                <ul class="treeview-menu">
                      <li class="<?php if($this->uri->segment(1,0)=='dashboard'){echo 'active';}else{echo '';}?>">
                        <a href="<?php echo site_url('dashboard/index') ?>"><i class="fa fa-area-chart"></i><span>Dashboard</span></a>
                    </li>
                      <li class="<?php if($this->uri->segment(1,0)=='kunjungan'){echo 'active';}else{echo '';}?>">
                        <a href="<?php echo site_url('kunjungan/index') ?>"><i class="fa fa-book"></i><span>Kunjungan</span></a>
                    </li>
                    <li class="<?php if($this->uri->segment(1,0)=='tujuan'){echo 'active';}else{echo '';}?>">
                        <a href="<?php echo site_url('tujuan/index') ?>"><i class="fa fa-location-arrow"></i><span>Tujuan</span></a>
                    </li>
                </ul>
            </li>

            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-thumbs-o-up"></i> <span>Survey Kepuasan</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if($this->uri->segment(1,0)=='dashboard_skm'){echo 'active';}else{echo '';}?>">
                        <a href="<?php echo site_url('dashboard_skm/index') ?>"><i class="fa fa-area-chart"></i><span>Dashboard</span></a>
                    </li>
                      <li class="<?php if($this->uri->segment(1,0)=='skm'){echo 'active';}else{echo '';}?>">
                        <a href="<?php echo site_url('skm/index') ?>"><i class="fa fa-book"></i><span>Angket Kepuasan</span></a>
                    </li>
                    <li class="<?php if($this->uri->segment(1,0)=='aspek_skm'){echo 'active';}else{echo '';}?>">
                        <a href="<?php echo site_url('aspek_skm/index') ?>"><i class="fa fa-sticky-note"></i><span>Penilaian</span></a>
                    </li>
                </ul>
            </li>


            <li class="<?php if($this->uri->segment(1,0)=='sirup'){echo 'treeview active';}else{echo 'treeview';}?>">
                <a href="<?php echo site_url('rekapsirup/index') ?>"><i class="fa fa-cloud-download"></i><span>Rekap Sirup</span></a>
            </li>
                
        </ul>
    </section>
                <!-- /.sidebar -->
</aside>