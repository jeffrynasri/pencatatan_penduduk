<!doctype html>
<html>

<head>
    <title>Buku Tamu</title>
     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <!-- Bootstrap 3.3.6 -->
     <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap.min.css');?>">
     <!-- Font Awesome -->
     <link rel="stylesheet" href="<?php echo site_url('resources/css/font-awesome.min.css');?>">
     <!-- Pretty RadioButton -->
    <link rel="stylesheet" href="<?php echo site_url('resources/css/pretty-checkbox.min.css');?>"/>
     <!-- Ionicons -->
     <link rel="stylesheet" href="<?php echo site_url('resources/css/ionicons.min.css');?>">
     <!-- Datetimepicker -->
     <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap-datetimepicker.min.css');?>">
     <!-- Theme style -->
     <link rel="stylesheet" href="<?php echo site_url('resources/css/AdminLTE.min.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo site_url('resources/css/_all-skins.min.css');?>">

    <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap-nav-centerred.css');?>">
    
    <link rel="stylesheet" href="<?php echo site_url('resources/css/jquery-ui.css');?>">

    <style type="text/css">
        body {
            background: #ecf0f1;
            font-family: Helvetica Neue;
        }
        
        h2 {
            margin-bottom: -5px;
        }
        
        p {
            font-size: 1.5em;
            color: #000000;
        }
        
        table {
            background: rgba(0,0,0,0.2);
            color: #fff;
            border: 0;
            padding: 20px;
            width: 548px;
            height: 100px;
            margin-top: 20px;
        }
        
        tr {
            background: none;
            border: none;
            color: #000000;
            height: 35px;
            font-family: Helvetica Neue;
        }
        
        td {
            background: none;
            color: #000000;
            border: none;
            outline: none;
            width: 548px;
            height: 80px;
            padding: 10px;
            float: left;
            font-family: Helvetica Neue;
            font-size: 16px; 
        }
        
        input.textbox {
            background: rgba(23, 20, 20, 0.52);
            border: none;
            border-radius: 3px;
            height: 25px;
            padding: 8px;
            padding-left: 37px;
            margin-bottom: 8px;
            transition: 1s all;
            -moz-transition: 1s all;
            -o-transition: 1s all;
        }
        textarea {
            background: rgba(23, 20, 20, 0.52);
            border: none;
            border-radius: 3px;
            height: 25px;
            padding: 8px;
            padding-left: 37px;
            margin-bottom: 8px;
            transition: 1s all;
            -moz-transition: 1s all;
            -o-transition: 1s all;
        }

        input:focus {
            border: 1px solid #000000;
            transition: 1s all;
            -moz-transition: 1s all;
            -o-transition: 1s all;
        }
        
        .button {
            background: #3498db;
            color: white;
            width: 495px;
            
            height: 40px;
            border: none;
            border-radius: 5px;
            transition: 1s all;
            -moz-transition: 1s all;
            -o-transition: 1s all;
        }
        
        .button:hover {
            background: #2980b9;
            transition: 1s all;
            -moz-transition: 1s all;
            -o-transition: 1s all;
        }
        
        .button-gambar {
            height: 35px;
        }


    </style>
</head>
<body>
    <div class="wrapper">
        <div class="sliding-background"></div>
    </div>
        <div align="center"></div>
        <ul class="nav nav-pills" width="548" border="0" align="center">
            <li class="active"><a href="#tab_default_1" data-toggle="tab">Buku Tamu</a></li>
            <li><a href="#tab_default_2" data-toggle="tab">Survey Kepuasan</a></li>
        </ul>
        
        <div class="tab-content">
            <div class="tab-pane active" id="tab_default_1">
                <?php echo form_open('home/index'); ?>
                <table width="548" border="0" align="center">
                    <tr>
                        <td colspan="2">
                            <div align="center">
                                <h2>BUKU TAMU</h2><style type="text/css">body {font-family: Georgia;} h2 {font-family: Stencil, monospace;}</style>
                                <p>.......................................................</p>
                            </div>
                        </td>
                    </tr>

                    <?php   
                        if(isset($_message)) $this->load->view($_message);
                    ?>
                
                        <td>
                            <div class="col-md-12">
                                <label for="nama" class="control-label"><span class="text-danger">*</span>Nama</label>
                                <div class="form-group">
                                    <input type="text" name="nama" value="" class="form-control" id="nama"/>
                                    <span class="text-danger"><?php echo form_error('nama');?></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <label for="no_hp" class="control-label"><span class="text-danger">*</span>No Hp</label>
                                <div class="form-group">
                                    <input type="number" name="no_hp" value="" class="form-control" id="no_hp" />
                                    <span class="text-danger"><?php echo form_error('no_hp');?></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <label for="instansi" class="control-label"><span class="text-danger">*</span>Instansi</label>
                                <div class="form-group">
                                    <input type="text" name="instansi" value="" class="form-control" id="instansi" />
                                    <span class="text-danger"><?php echo form_error('instansi');?></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <label for="id_tujuan" class="control-label"><span class="text-danger">*</span>Tujuan</label>
                                <div class="form-group">
                                    <select name="id_tujuan" class="form-control">
                                        <?php 
                                        foreach($all_tujuan as $tujuan)
                                        {
                                            $selected = ($tujuan['id'] == $this->input->post('id_tujuan')) ? ' selected="selected"' : "";

                                            echo '<option value="'.$tujuan['id'].'" '.$selected.'>'.$tujuan['nama'].'</option>';
                                        } 
                                        ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('id_tujuan');?></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <label for="keterangan" class="control-label"><span class="text-danger">*</span>Keterangan</label>
                                <div class="form-group">
                                    <input type="text" name="keterangan" value="" class="form-control" id="keterangan" />
                                    <span class="text-danger"><?php echo form_error('keterangan');?></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <label for="email" class="control-label">Email</label>
                                <div class="form-group">
                                    <input type="text" name="email" value="" class="form-control" id="email" />
                                    <span class="text-danger"><?php echo form_error('email');?></span>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div align="center">
                                <input class="button" type="submit" name="submit" value="Simpan"> </div>
                        </td>
                    </tr>


                </table>
                <?php echo form_close(); ?>
            </div>
            
            
            
            <div class="tab-pane" id="tab_default_2">
                <?php echo form_open('home/skm'); ?> 
                 <table border="0" align="center" width="548">
                    
                     <?php foreach($all_aspek_skm as $aspek) { ?>

                         <tr>
                            <td>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label"><?php echo $aspek['nama'];?></label>
                                    <div class="col-sm-12">
                                        <label class="radio-inline"> <input type="radio" name="<?php  echo str_replace(" ","_",$aspek['nama']) ?>" value="1" checked> Tidak Puas </label>
                                        <label class="radio-inline"> <input type="radio" name="<?php  echo str_replace(" ","_",$aspek['nama']) ?>" value="2"> Kurang Puas </label>
                                        <label class="radio-inline"> <input type="radio" name="<?php  echo str_replace(" ","_",$aspek['nama']) ?>" value="3"> Cukup Puas</label>
                                        <label class="radio-inline"> <input type="radio" name="<?php  echo str_replace(" ","_",$aspek['nama']) ?>"  value="4" > Puas </label>
                                        <label class="radio-inline"> <input type="radio" name="<?php  echo str_replace(" ","_",$aspek['nama']) ?>"  value="5" > Sangat Puas </label>
                                       
                                    </div>
                                </div>    
                            </td>
                        </tr>
                    <?php }?>
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <label for="saran" class="control-label">Saran</label>
                                <div class="form-group">
                                    <input  type="text" name="saran" value="" class="form-control" id="saran">
                                     <!-- <textarea type="text" name="saran" value="" class="form-control" rows="3" id="saran"></textarea> -->
                                    <span class="text-danger"><?php echo form_error('saran');?></span>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div align="center">
                                <input class="button" type="submit" name="submit" value="Simpan"> </div>
                        </td>
                    </tr>
                 </table>
                <?php echo form_close(); ?>
            </div>
             
        </div>
        
    <?php echo form_close(); ?> 

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo site_url('resources/js/jquery-2.2.3.min.js');?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo site_url('resources/js/bootstrap.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?php echo site_url('resources/js/fastclick.js');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo site_url('resources/js/app.min.js');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo site_url('resources/js/demo.js');?>"></script>
    <!-- DatePicker -->
    <script src="<?php echo site_url('resources/js/moment.js');?>"></script>
    <script src="<?php echo site_url('resources/js/bootstrap-datetimepicker.min.js');?>"></script>
    <script src="<?php echo site_url('resources/js/global.js');?>"></script>

    <script src="<?php echo site_url('resources/js/jquery-ui.js');?>"></script>

     <script type="text/javascript">
        $(document).ready(function(){
            $( "#nama" ).autocomplete({
                source: "<?php echo site_url('home/get_autocomplete_name/?');?>"
            });
            $( "#no_hp" ).autocomplete({
                source: "<?php echo site_url('home/get_autocomplete_nohp/?');?>"
            });
             $( "#instansi" ).autocomplete({
                source: "<?php echo site_url('home/get_autocomplete_instansi/?');?>"
            });
             $( "#email" ).autocomplete({
                source: "<?php echo site_url('home/get_autocomplete_email/?');?>"
            });
            $( "#keterangan" ).autocomplete({
                source: "<?php echo site_url('home/get_autocomplete_keterangan/?');?>"
            });
        });
    </script>

</body>

</html>  

