<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Penilaian</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('penilaian/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>Id Skm</th>
						<th>Id Aspek Skm</th>
						<th>Nilai</th>
						<th>Aksi</th>
                    </tr>
                    <?php foreach($penilaian as $p){ ?>
                    <tr>
						<td><?php echo $p['id']; ?></td>
						<td><?php echo $p['id_skm']; ?></td>
						<td><?php echo $p['id_aspek_skm']; ?></td>
						<td><?php echo $p['nilai']; ?></td>
						<td>
                            <a href="<?php echo site_url('penilaian/edit/'.$p['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Ubah</a> 
                            <a href="<?php echo site_url('penilaian/remove/'.$p['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Hapus</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
