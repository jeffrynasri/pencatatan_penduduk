<div class="box-body">
    <div class="col-md-12">
        <div class="box" style="padding: 15px">
            <div class="box-header">
                <h3 class="box-title">Dashboard</h3>
            </div>

            <?php echo form_open('dashboard') ?>  
            <div class="row" style="margin-bottom: 6px;">
              <div class="col-sm-2">       
                <div class="form-group">
                    <select class="form-control mb-1" name="tgl">
                      <option value="">TANGGAL</option>
                       <?php
                          for($i=1;$i<=31;$i++){
                            if($i==$tanggal_sekarang){
                              echo "<option value='". $i . "' selected>". $i ."</option>";  
                            }else{
                              echo "<option value='". $i . "'>". $i ."</option>";
                            }
                            
                          }
                        ?>
                      
                    </select>
                </div>
              </div>   
            <div class="row" style="margin-bottom: 6px;">

              <div class="col-sm-2">       
                <div class="form-group">
                    <select class="form-control mb-1" name="bulan">
                      <option value="">BULAN</option>
                      <option value="1" <?php if($bulan_sekarang==1){echo "selected";}?> >Januari</option>
                      <option value="2" <?php if($bulan_sekarang==2){echo "selected";}?> >Februari</option>
                      <option value="3" <?php if($bulan_sekarang==3){echo "selected";}?> >Maret</option>
                      <option value="4" <?php if($bulan_sekarang==4){echo "selected";}?> >April</option>
                      <option value="5" <?php if($bulan_sekarang==5){echo "selected";}?> >Mei</option>
                      <option value="6" <?php if($bulan_sekarang==6){echo "selected";}?> >Juni</option>
                      <option value="7" <?php if($bulan_sekarang==7){echo "selected";}?> >Juli</option>
                      <option value="8" <?php if($bulan_sekarang==8){echo "selected";}?> >Agustus</option>
                      <option value="9" <?php if($bulan_sekarang==9){echo "selected";}?> >September</option>
                      <option value="10" <?php if($bulan_sekarang==10){echo "selected";}?> >Oktober</option>
                      <option value="11" <?php if($bulan_sekarang==11){echo "selected";}?> >November</option>
                      <option value="12" <?php if($bulan_sekarang==12){echo "selected";}?> >Desember</option>
                    </select>
                </div>
              </div>
              <div class="col-sm-2">       
                <div class="form-group">
                    <select class="form-control mb-1" name="tahun">
                      <option value="">TAHUN</option>
                      <?php
                            $thn_skr = date('2021');
                            for ($x = $thn_skr; $x >= 2019; $x--) {
                          ?>
                        <option value="<?php echo $x ?>" <?php if($tahun_sekarang==$x){echo "selected";}?> ><?php echo $x ?></option>
                      <?php } ?>
                    </select>
                </div>
              </div>
              <div class="col-sm-1">
                <button class="btn btn-primary" name="cari" id="btn_cari">Cari</button>
              </div>
            </div>
        
            <?php echo form_close(); ?>

                <div class="row">
                	
                	<div class="col-lg-4 col-xs-6">
                		<div class="small-box bg-green">
                            <div class="inner">
                                <h3>
                                	<?php echo isset($total_bidang[0]['jumlah']) ? $total_bidang[0]['jumlah']:  "0" ;?>
                                </h3>
                                <p>Bidang</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-book"></i>
                            </div>
                        </div>
                	</div>
                  <div class="col-lg-4 col-xs-6">
                    <div class="small-box bg-red">
                            <div class="inner">
                                <h3>
                                  <?php echo isset($total_sekretariat[0]['jumlah']) ? $total_sekretariat[0]['jumlah']:  "0" ;?>
                                </h3>
                                <p>Sekretariat</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-book"></i>
                            </div>
                        </div>
                  </div>
                    <div class="col-lg-4 col-xs-6">
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3>
                                	<?php echo isset($total_lpse[0]['jumlah']) ? $total_lpse[0]['jumlah']:  "0" ;?>
                                </h3>
                                <p>LPSE</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-book"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <!-- Diagram -->

                  <div class="col-md-12">
                      <div id="graft" style="height: 400px; margin: 0 auto"></div>
                  </div>

                  <!-- End Diagram -->


                  <?php if($lihat_data == 1){ ?>
                    <!-- <div id='grafik'></div>
                    
                    <?php 
                      echo "<script type='text/javascript' language='javascript'>tampilkan_chart(".json_encode($data_grafik).")</script>";
                    ?> -->
                    
                    <table id='example2' class='table table-bordered table-hover text-center'>
                      <thead>
                        <tr>
                          <th class='col-md-4'>Tanggal/Jam</th>
                          <th class='col-md-4'>Nama</th>
                          <th class='col-md-4'>Jumlah Tamu</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($data_grafik as $row) { ?>
                              <tr>
                                  <td><?php echo $row['tanggal'] ?></td>
                                  <td><?php echo $row['namak'] ?></td>
                                  <td><?php echo $row['jumlah'] ?></td>
                              </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  <?php }else{ ?>
                    <h5 class='text-center'>Data Tidak Ditemukan</h5>
                    <div id="chart" align="center" style="width: 500px; height: 300px; margin: 0 auto;"/>
                  <?php } ?>
                </div>

          
            </div>
        </div>
    </div>
</div>

<body>
    <?php if (empty($data_grafik)) { ?>

    <?php } else { ?>
      <?php foreach ($data_grafik as $dg) {
        $tanggal[] = intval($dg['tanggal']);
        $jumlah[] = intval($dg['jumlah']);
        $nama[] = $dg['namak'];
      } ?>
      <script>
            Highcharts.chart('graft', {
              title: {
                  text: ''
              },
              xAxis: {
                  categories: <?php echo json_encode($nama); ?>,
                  title: {
                    text: ''
                }
              },
              yAxis: {
                title: {
                  text: 'Jumlah'
                },
                tickInterval: 1
              },
              credits: {
                  enabled: false
              },
              plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
              },
              labels: {
                  items: [{
                      style: {
                          left: '50px',
                          top: '18px',
                          color: ( // theme
                              Highcharts.defaultOptions.title.style &&
                              Highcharts.defaultOptions.title.style.color
                          ) || 'black'
                      }
                  }]
              },
              series: [{
                  colorByPoint: true,
                  type: 'column',
                  name: 'Pengunjung',
                  data: <?php echo json_encode($jumlah); ?>
              }]
          });

          </script>
      <?php } ?>

    <!-- js untuk jquery -->
  <script src="js/jquery-1.11.2.min.js"></script>
  <!-- js untuk bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- js untuk moment -->
  <script src="js/moment.js"></script>
  <!-- js untuk bootstrap datetimepicker -->
  <script src="js/bootstrap-datetimepicker.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
         $("#btn_cari").submit();
       $('#tanggal').datetimepicker({
        format : 'DD/MM/YYYY'
       });
    });
  </script>
</body>