<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Skm</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('skm/add'); ?>" class="btn btn-success btn-sm">Tambah</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Tanggal</th>
                        <th>Penilaian</th>
                        <th>Saran</th>
						<th>Aksi</th>
                    </tr>
                    <?php 
                        $CI =& get_instance();
                        $CI->load->model('Penilaian_model');
                    ?>

                    <?php foreach($skm as $s){ ?>
                    <tr>
                        <td><?php echo $s['tanggal']; ?></td>
                        <td>
                            <?php $result= $CI->Penilaian_model->get_penilaian_byskm($s['id']);?>
                            <ul>
                                <?php foreach($result as $re){ ?>
                                    <li><?php echo $re['nama']; ?> : <?php echo $re['nilai']; ?></li>
                                <?php } ?>
                            </ul>
                        </td>
						<td><?php echo $s['saran']; ?></td>
						<td>
                            <a href="<?php echo site_url('skm/edit/'.$s['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Ubah</a> 
                            <a href="<?php echo site_url('skm/remove/'.$s['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Hapu</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
