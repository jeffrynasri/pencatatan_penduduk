
<div class="box-body">
  <div class="col-md-12">
    <div class="box" style="padding: 15px">
      <div class="box-header">
        <h3 class="box-title">Rekap Sirup</h3>
      </div>

      <p><span class="text-danger">*</span>Data diambil dari https://sirup.lkpp.go.id/sirup/ro/rekap. Rekap data berupa jumlah paket dan jumlah pagu RUP pada OPD</p>
      <div class="panel panel-primary">
        <div class="panel-heading">Rekap Banyak</div>
        <div class="panel-body">
          <div class="col-md-12">
            <a class="pull-right" href="<?php echo base_url().'assets/template.xlsx'?>">Download Template</a>
          </div>
          <a href="<?php echo site_url('rekapsirup/generate/'); ?>" class="btn btn-success">
            <i class="fa fa-download"></i> Rekap</a>
            <?php 
            if(isset($isAvailibleDownload)){?>
              <a href="<?php echo base_url().'assets/hasil.xlsx'?>">Hasil Rekap</a>
            <?php }?>

            <br>
            <p><span class="text-danger">*</span>Hasil Berupa Excell</p>
          </div>
        </div>
        <div class="panel panel-primary">
          <div class="panel-heading">Rekap Satuan</div>
          <div class="panel-body">
            <?php echo form_open('rekapsirup/hitung'); ?>
            <div class="box-body">
              <div class="row clearfix">
               <div class="col-md-12">
                <label for="tahun" class="control-label"><span class="text-danger">*</span>Tahun</label>
                <div class="form-group">
                 <input type="text" name="tahun" value="2019" class="form-control" id="tahun" />
                 <span class="text-danger"><?php echo form_error('tahun');?></span>
               </div>
             </div>
             <div class="col-md-12">
              <label for="id_satker" class="control-label"><span class="text-danger">*</span>Id Satker</label>
              <div class="form-group">
               <input type="number" step="1" name="id_satker" value="<?php echo $this->input->post('id_satker'); ?>" class="form-control" id="id_satker" />
               <span class="text-muted">Contoh Pengisian : 69813</span>
               <span class="text-danger"><?php echo form_error('id_satker');?></span>
               <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Daftar ID Satker</
             </div>
           </div>
         </div>
       </div>
       <div class="box-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-download"></i> Rekap </button>
        <br>
        <p><span class="text-danger">*</span>Hasil Berupa Tampilan Website</p>
       </div>
     <?php echo form_close(); ?>

     <!-- //////TOTAL PAKET BOZ -->
     <div class="row">
       <div class="col-md-2">
          <h5>Total Paket Tender</h5>                 
          <h5>
            <?php 
            if( isset($total_paket_penyedia)){
              echo $total_paket_penyedia['tender'] . " Paket";
            }else{
              echo "0 Paket";
            }
            ?>
          </h5>                   
        </div>
        <div class="col-md-2">
          <h5>Total Paket Non Tender</h5>                        
          <h5>
            <?php 
            if(isset($total_paket_penyedia)){
              echo $total_paket_penyedia['nontender']  . " Paket";
            }else{
              echo "0 Paket";
            }
            ?>
          </h5>                   
        </div>
        <div class="col-md-2">
          <h5>Total Paket E-Purchasing</h5>                        
          <h5>
            <?php 
            if(isset($total_paket_penyedia)){
              echo $total_paket_penyedia['epurchasing']   . " Paket";
            }else{
              echo "0 Paket";
            }
            ?>
          </h5>                   
        </div>
        <div class="col-md-2">
          <h5>Total Paket Yang Lain</h5>                        
          <h5>
            <?php 
            if(isset($total_paket_penyedia)){
              echo $total_paket_penyedia['lain'];
            }else{
              echo "0 Paket";
            }
            ?>
          </h5>                   
        </div>
        <div class="col-md-2">
          <h5>Total Paket Swakelola</h5>                        
          <h5>
            <?php 
            if(isset($total_paket_swakelola)){
              echo $total_paket_swakelola  . " Paket";
            }else{
              echo "0 Paket";
            }
            ?>
          </h5>                   
        </div>
         <div class="col-md-2">
          <h5>Total Paket Penyedia Dalam Swakelola</h5>                        
          <h5>
            <?php 
            if(isset($total_paket_penyedia_dalamswakelola)){
              echo $total_paket_penyedia_dalamswakelola  . " Paket";
            }else{
              echo "0 Paket";
            }
            ?>
          </h5>                   
        </div>
     </div>
     

     <!-- /////////Total Pagu boZZ -->
     <div class="row">
       <div class="col-md-2">
            <h5>Total Pagu Tender</h5>     
            <h5>
             <?php 
             if( isset($total_pagu_penyedia)){
               echo "Rp. ". number_format($total_pagu_penyedia['tender']). ",00";
             }else{
               echo "0";
             }
             ?>
           </h5>      
         </div>
         <div class="col-md-2">
          <h5>Total Pagu Non Tender</h5>      
          <h5>
           <?php 
           if(isset($total_pagu_penyedia)){
             echo "Rp. ".number_format($total_pagu_penyedia['nontender']). ",00";
           }else{
             echo "0";
           }
           ?>
         </h5>      
       </div>
        <div class="col-md-2">
          <h5>Total Pagu E-Purchasing</h5>      
          <h5>
           <?php 
           if(isset($total_pagu_penyedia)){
             echo "Rp. ".number_format($total_pagu_penyedia['epurchasing']). ",00";
           }else{
             echo "0";
           }
           ?>
         </h5>      
       </div>
        <div class="col-md-2">
          <h5>Total Pagu Paket Yang Lain</h5>      
          <h5>
           <?php 
           if(isset($total_pagu_penyedia)){
             echo ($total_pagu_penyedia)['lain'];
           }else{
             echo "0";
           }
           ?>
         </h5>      
       </div>
        <div class="col-md-2">
          <h5>Total Pagu Swakelola</h5>      
          <h5>
           <?php 
           if(isset($total_pagu_swakelola)){
             echo "Rp. ".number_format($total_pagu_swakelola). ",00";
           }else{
             echo "0";
           }
           ?>
         </h5>      
       </div>
       <div class="col-md-2">
          <h5>Total Pagu Penyedia Dalam Swakelola</h5>      
          <h5>
           <?php 
           if(isset($total_pagu_penyedia_dalamswakelola)){
             echo "Rp. ".number_format($total_pagu_penyedia_dalamswakelola). ",00";
           }else{
             echo "0";
           }
           ?>
         </h5>      
       </div>
     </div>
     

 
</div>
</div>
</div> 

</div>
</div>


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Daftar ID Satker</h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped">
            <tr>
              <th>ID Satker</th>
              <th>Nama</th>
            </tr>
            <tr>
              <td>69808</td>
              <td>BADAN PENDAPATAN, PENGELOLAAN KEUANGAN DAN ASET DAERAH</td>
            </tr>
            <tr>
              <td>69813</td>
              <td>BADAN KEPEGAWAIAN DAERAH</td>
            </tr>
            <tr>
              <td>69811</td>
              <td>BADAN PERENCANAAN PEMBANGUNAN, PENELITIAN DAN PENGEMBANGAN DAERAH</td>
            </tr>
            <tr>
              <td>69809</td>
              <td>BADAN PENANGGULANGAN BENCANA DAERAH</td>
            </tr>
            <tr>
              <td>69888</td>
              <td>DINAS SATUAN POLISI PAMONG PRAJA</td>
            </tr>
            <tr>
                <td>97123</td>
                <td>DINAS LINGKUNGAN HIDUP</td>
            </tr>
            <tr>
                <td>97126</td>
                <td>DINAS  KELUARGA BERENCANA, PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK</td>
            </tr>
            <tr>
                <td>101500</td>
                <td>DINAS PARIWISATA DAN KEBUDAYAAN</td>
            </tr>
            <tr>
                <td>100381</td>
                <td>DINAS PEMUDA & OLAHRAGA</td>
            </tr>
            <tr>
                <td>69789</td>
                <td>DINAS KEPENDUDUKAN,CATATAN SIPIL</td>
            </tr>
            v
            <tr>
                <td>97126</td>
                <td>DINAS KESEHATAN</td>
            </tr>
            <tr>
                <td>96015</td>
                <td>DINAS KOMUNIKASI DAN INFORMATIKA</td>
            </tr>
            <tr>
                <td>97127</td>
                <td>DINAS KOPERASI, USAHA MIKRO, PERINDUSTRIAN DAN PERDAGANGAN </td>
            </tr>
            <tr>
                <td>69798</td>
                <td>DINAS PERHUBUNGAN </td>
            </tr>
            <tr>
                <td>97121</td>
                <td>DINAS PERUMAHAN & KAWASAN PERMUKIMAN</td>
            </tr>
            <tr>
                <td>97122</td>
                <td>DINAS PENANAMAN MODAL & PTSP</td>
            </tr>
            <tr>
                <td>69794</td>
                <td>DINAS PENDIDIKAN</td>
            </tr>
            <tr>
                <td>97124</td>
                <td>DINAS PEMBERDAYAAN MASYARAKAT & DESA</td>
            </tr>

            <tr>
                <td>101836</td>
                <td>DINAS PERTANAHAN</td>
            </tr>
              

            <tr>
                <td>97130</td>
                <td>DINAS PERTANIAN</td>
            </tr>

            <tr>
                <td>97128</td>
                <td>DINAS PERIKANAN</td>
            </tr>


            <tr>
                <td>69806</td>
                <td>DINAS TENAGA KERJA</td>
            </tr>


            <tr>
                <td>98115</td>
                <td>DINAS PERPUSTAKAAN DAN KEARSIPAN</td>
            </tr>

            <tr>
                <td>69799</td>
                <td>DINAS PEKERJAAN UMUM DAN TATA RUANG</td>
            </tr>


            <tr>
                <td>69802</td>
                <td>DINAS SOSIAL</td>
            </tr>


            <tr>
                <td>69861</td>
                <td>KANTOR KESATUAN BANGSA DAN POLITIK</td>
            </tr>


            <tr>
                <td>97114</td>
                <td>BAGIAN KESEJAHTERAAN RAKYAT</td>
            </tr>


            <tr>
                <td>97119</td>
                <td>BAGIAN KEUANGAN SETDA </td>
            </tr>


            <tr>
                <td>97115</td>
                <td>BAGIAN PROGRAM PEMBANGUNAN SETDA</td>
            </tr>


            <tr>
                <td>97113</td>
                <td>BAGIAN PEMERINTAHAN SETDA</td>
            </tr>


            <tr>
                <td>97116</td>
                <td>BAGIAN PEREKONOMIAN DAN SDA SETDA</td>
            </tr>


            <tr>
                <td>69760</td>
                <td>BAGIAN HUKUM SETDA</td>
            </tr>


            <tr>
                <td>97118</td>
                <td>BAGIAN HUMAS DAN PROTOKOL SETDA</td>
            </tr>


            <tr>
                <td>69759</td>
                <td>BAGIAN ORTALA SETDA</td>
            </tr>


            <tr>
                <td>97117</td>
                <td>BAGIAN LAYANAN PENGADAAN BARANG/JASA SETDA</td>
            </tr>


            <tr>
                <td>100847</td>
                <td>BAGIAN UMUM SETDA</td>
            </tr>

            <tr>
                <td>69924</td>
                <td>KECAMATAN GRESIK</td>
            </tr>


            <tr>
                <td>69906</td>
                <td>KECAMATAN BALONGPANGGANG</td>
            </tr>


            <tr>
                <td>69910</td>
                <td>KECAMATAN BENJENG</td>
            </tr>

            <tr>
                <td>69912</td>
                <td>KECAMATAN BUNGAH</td>
            </tr>


            <tr>
                <td>69915</td>
                <td>KECAMATAN CERME</td>
            </tr>


            <tr>
                <td>69916</td>
                <td>KECAMATAN DRIYOREJO</td>
            </tr>

            <tr>
                <td>69920</td>
                <td>KECAMATAN DUDUK SAMPEYAN</td>
            </tr>

            <tr>
                <td>69923</td>
                <td>KECAMATAN DUKUN</td>
            </tr>

            <tr>
                <td>69927</td>
                <td>KECAMATAN KEBOMAS</td>
            </tr>

            <tr>
                <td>69929</td>
                <td>KECAMATAN KEDAMEAN</td>
            </tr>

            <tr>
                <td>69930</td>
                <td>KECAMATAN MANYAR</td>
            </tr>


            <tr>
                <td>69932</td>
                <td>KECAMATAN MENGANTI</td>
            </tr>


            <tr>
                <td>69934</td>
                <td>KECAMATAN PANCENG</td>
            </tr>

            <tr>
                <td>69937</td>
                <td>KECAMATAN SIDAYU</td>
            </tr>


            <tr>
                <td>69939</td>
                <td>KECAMATAN UJUNGPANGKAH</td>
            </tr>


            <tr>
                <td>69938</td>
                <td>KECAMATAN TAMBAK</td>
            </tr>


            <tr>
                <td>69935</td>
                <td>KECAMATAN SANGKAPURA</td>
            </tr>


            <tr>
                <td>69941</td>
                <td>KECAMATAN WRINGINANOM</td>
            </tr>


            <tr>
                <td>74059</td>
                <td>RSUD IBNU SINA</td>
            </tr>

            <tr>
                <td>69858</td>
                <td>SEKRETARIAT DPRD</td>
            </tr>


            <tr>
                <td>69943</td>
                <td>INSPEKTORAT</td>
            </tr>
          </table>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>




