<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Buku Tamu | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/font-awesome.min.css');?>">
        <!-- Admin LTE 2.0.5 -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/AdminLTE.min.css');?>">
        <!-- iCheck -->
        <!-- <link rel="stylesheet" href="<?php echo site_url('resources/css/blue.css');?>"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="jquery.js"></script>
        <body class="bg">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/background.css'?>">

    <style type="text/css">
        body {
            background: : #ecf0f1;
            font-family: Times new roman;
        }

        h2 {
            margin: 20px; 
            font-size: 40px;
        }

        table {
            background: rgba(0,0,0,0.2);
            color: #fff;
            border: 0;
            padding: 20px;
            width: 348px;
            height: 100px;
            margin-top: 100px;
            z-index: 1;
        }
        tr {
            z-index: 2;
            background: none;
            border: none;
            color: #000000;
            height: 35px;
            font-family: Arial;
        }
        
        td {
            z-index: 2;
            background: none;
            color: #000000;
            border: none;
            outline: none;
            width: 348px;
            height: 70px;
            padding: 15px;
            float: left;
            font-family: Arial;
            font-size: 16px; 
        }

        .btn {
            background: #3498db;
            width: 100px;
            margin-top: -20px;
            margin-left: -10px;
            height: 30px;
            padding: 5px;
            border: none;
            border-radius: 5px;
            transition: 1s all;
            -moz-transition: 1s all;
            -o-transition: 1s all;
        }
        
        .checkbox {
            width: 93%;
            padding: 10px 15px;
            font-size: 14pt;
            border: 1px;
            color: #2D3E50;
        }
        .form-checkbox {
            width: 93%;
            padding: 10px 15px;
            font-size: 14pt;
            border: 1px;
            color: #2D3E50;
        }
         #mybutton {
            position: relative;
            z-index: 999;
            left: 91%;
            top: -25px;
            cursor: pointer;
         }
    </style>
 </head>
    <body class="login-page">
        <?php   if(isset($_errorlogin))
            $this->load->view($_errorlogin);
        ?>
    <div align="center"></div>
     <table width="366" border="0" align="center">
        <tr>
            <td colspan="2">
                <div align="center">
                <div class="login-logo col-sm-12">
                    <h2>BUKU TAMU</h2><style type="text/css">body {font-family: Georgia;} h2 {font-family: Stencil, monospace;}</style>
                </div><!-- /.login-logo -->
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="table table-sm"></div>
                <form action="<?php echo site_url('home/login') ?>" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Username" id="username" name="username" />
                        <span class="glyphicon glyphicon-font form-control-feedback"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" id="pass" name="password" />
                        <span id="mybutton" onclick="change()"><i class="glyphicon glyphicon-eye-open"></i></span>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="col-xs-8"></div>
                    <div class="col-xs-4 text-center">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
                    </div><!-- /.col -->
                </td>
            </tr>        
        </form>

        </div><!-- /.login-box -->
    </table>
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo site_url('resources/js/jquery-2.2.3.min.js');?>"></script>

        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo site_url('resources/js/bootstrap.min.js');?>"></script>
        <!-- iCheck -->
        <script src="<?php echo site_url('resources/js/icheck.min.js');?>"></script>

        <script>
         function change() {
            var x = document.getElementById('pass').type;
 
            if (x == 'password')
            {
               document.getElementById('pass').type = 'text';
               document.getElementById('mybutton').innerHTML = '<i class="glyphicon glyphicon-eye-close"></i>';
            }
            else
            {
               document.getElementById('pass').type = 'password';
               document.getElementById('mybutton').innerHTML = '<i class="glyphicon glyphicon-eye-open"></i>';
            }
         }
      </script>



    </body>
</html>