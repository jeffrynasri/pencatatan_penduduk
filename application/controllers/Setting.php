<?php

include_once APPPATH . '/core/Admin_controller.php';
class Setting extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Setting_model');
    } 

    /*
     * Listing of setting
     */
    function index()
    {
        $id="1";
        $data['setting'] = $this->Setting_model->get_setting($id);
        
        if(isset($data['setting']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password_dewa','Password','required');
           
			if($this->form_validation->run())     
            {   
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $params = array(
                        'pengumuman' => $this->input->post('pengumuman'),
                        'is_popup_active' => $this->input->post('is_popup_active'),
                        'is_camera_active' => $this->input->post('is_camera_active'),
                    );
    
                    $this->Setting_model->update_setting($id,$params);            
                    redirect('setting/index');   
                }
            }
            $data['_view'] = 'setting';
            $data['_header']='layouts/admin_header';
            $data['_sidebar']='layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
        else
            show_error('The setting you are trying to edit does not exist.');
    }


    private function recreate_harisvaksin($limit_vaksin,$rentang_awal,$rentang_akhir){
        $emptyreturn=$this->Haris_vaksin_model->empty_haris_vaksin();
        for($i=$rentang_awal;$i<=$rentang_akhir;){
            $addreturn=$this->Haris_vaksin_model->add_haris_vaksin(array(
                'tanggal'=>$i,
                'is_active'=>1,
                'sisa_vaksin'=>$limit_vaksin
            ));
            $i=date('Y-m-d',strtotime($i.' + 1 days'));
        }
    }
}
