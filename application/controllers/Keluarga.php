<?php

 include_once APPPATH . '/core/Admin_controller.php';
class Keluarga extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penduduk_model');
        $this->load->model('Tweb_penduduk_kawin_model');
        $this->load->model('Tweb_penduduk_pekerjaan_model');
        $this->load->model('Tweb_penduduk_pendidikan_kk_model');
        $this->load->model('Tweb_penduduk_hubungan_model');
        $this->load->model('Tweb_penduduk_warganegara_model');
        $this->load->model('Tweb_status_dasar_model');
        $this->load->model('Tweb_golongan_darah_model');
        $this->load->model('Tweb_penduduk_agama_model');
        $this->load->model('Tweb_kecamatan_model');
        $this->load->model('Tweb_desa_model');
        $this->load->model('Setting_model');
    } 

    /*
     * Listing of penduduk
     */
    function index()
    {
        $data['all_kecamatan']=$this->Tweb_kecamatan_model->get_all_tweb_kecamatan();
        $data['all_desa']=$this->Tweb_desa_model->get_all_tweb_desa();
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';

        $data['_view'] = 'keluarga/index';
        $this->load->view('layouts/admin_template',$data);
    }
    function detail($no_kk){
        $data['no_kk']=$no_kk;
        $data['keluarga']=$this->Penduduk_model->get_all_penduduk('penduduk.*',array('no_kk'=>$no_kk));
        $data['_header']='layouts/admin_header';
        $data['_sidebar']='layouts/admin_sidebar';
        $data['_view']='keluarga/detail';
        $this->load->view('layouts/admin_template',$data);
    }
  
    function get_desa_by_idkecamatan_json(){
        $id_kecamatan=$this->input->post("id_kecamatan");
        $listdesa = $this->Tweb_desa_model->get_all_tweb_desa(array('district_id'=>$id_kecamatan));
        echo json_encode($listdesa);
    }
    function get_data_keluarga_json()
    {
        $params_where=array();
 
        if($this->input->post('id_desa')!='-1'){
            $params_where['id_desa']=$this->input->post('id_desa');
        }
        if($this->input->post('id_kecamatan')!='-1'){
            $params_where['id_kecamatan'] = $this->input->post('id_kecamatan');
        }
        
        $list = $this->Penduduk_model->get_datatables($params_where,'count(penduduk.id) as jumlah,no_kk','penduduk.nama','no_kk');
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $field->no_kk;
            $row[] = $field->jumlah;
           // $row[] = $field->nama_desa;
            //$row[] = $field->nama_kecamatan;

            $row[] = "<a href='" .  'detail/'. $field->no_kk ."'" . "class='btn btn-warning btn-xs'><span class='fa fa-arrow-right'></span> Detail</a>";
            $data[] = $row;
        }
        //echo $this->Penduduk_model->count_all($params_where);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Penduduk_model->count_all($params_where,'count(penduduk.id) as jumlah,no_kk','penduduk.nama','no_kk'),
            "recordsFiltered" => $this->Penduduk_model->count_filtered($params_where,'count(penduduk.id) as jumlah,no_kk','penduduk.nama','no_kk'),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
