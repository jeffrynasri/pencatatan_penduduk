<?php

 include_once APPPATH . '/core/Admin_controller.php';
class Penduduk extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penduduk_model');
        $this->load->model('Tweb_penduduk_kawin_model');
        $this->load->model('Tweb_penduduk_pekerjaan_model');
        $this->load->model('Tweb_penduduk_pendidikan_kk_model');
        $this->load->model('Tweb_penduduk_hubungan_model');
        $this->load->model('Tweb_penduduk_warganegara_model');
        $this->load->model('Tweb_status_dasar_model');
        $this->load->model('Tweb_golongan_darah_model');
        $this->load->model('Tweb_penduduk_agama_model');
        $this->load->model('Tweb_kecamatan_model');
        $this->load->model('Tweb_desa_model');
        $this->load->model('Tweb_status_bantuan_model');
        $this->load->model('Setting_model');
    } 

    /*
     * Listing of penduduk
     */
    function index()
    {
        $data['all_kecamatan']=$this->Tweb_kecamatan_model->get_all_tweb_kecamatan();
        $data['all_desa']=$this->Tweb_desa_model->get_all_tweb_desa();
        $data['_view'] = 'penduduk/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';

        $data['_view'] = 'penduduk/index';
        $this->load->view('layouts/admin_template',$data);
    }
    /*
     * Listing of penduduk
     */
    function detail($id)
    {
        $penduduk=$this->Penduduk_model->get_penduduk(array('penduduk.id' => $id));
        if(isset($penduduk['id'])){    
            $data['penduduk']=$penduduk;
            $data['setting'] = $this->Setting_model->get_setting('1');
            $data['all_desa'] = $this->Tweb_desa_model->get_all_tweb_desa();
            $data['all_kecamatan'] = $this->Tweb_kecamatan_model->get_all_tweb_kecamatan();
            $data['all_status_kawin'] = $this->Tweb_penduduk_kawin_model->get_all_tweb_penduduk_kawin();
            $data['all_pekerjaan'] = $this->Tweb_penduduk_pekerjaan_model->get_all_tweb_penduduk_pekerjaan();
            $data['all_pendidikan'] = $this->Tweb_penduduk_pendidikan_kk_model->get_all_tweb_penduduk_pendidikan_kk();
            $data['all_status_warga'] = $this->Tweb_penduduk_warganegara_model->get_all_tweb_penduduk_warganegara();
            $data['all_golongan_darah'] = $this->Tweb_golongan_darah_model->get_all_tweb_golongan_darah();
            $data['all_agama'] = $this->Tweb_penduduk_agama_model->get_all_tweb_penduduk_agama();
            $data['all_hubungan'] = $this->Tweb_penduduk_hubungan_model->get_all_tweb_penduduk_hubungan(); 
            $data['all_status_bantuan']= $this->Tweb_status_bantuan_model->get_all_tweb_status_bantuan();
            $data['_view'] = 'penduduk/index';
            $data['_header'] = 'layouts/admin_header';
            $data['_sidebar'] = 'layouts/admin_sidebar';

            $data['_view'] = 'penduduk/detail';
            $this->load->view('layouts/admin_template',$data);
        }else{
            show_error("Data Tdiak Ditemukan");
        }
    }

    // Tambah
    function add()
    {
        
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nik','NIK','required|integer|min_length[16]|max_length[16]');
        $this->form_validation->set_rules('no_kk','No KK','required|integer|min_length[16]|max_length[18]');
        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('no_kk','No KK','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
        if($this->form_validation->run())     
        {   
            if($this->is_nik_registered($this->input->post('nik'))){
                $this->session->set_flashdata('pesan', "NIK Telah Terdaftar");
            }else{
                date_default_timezone_set("Asia/Bangkok");
                $params = array(
                'id' => str_replace("-","",$this->uuid->v4()),
                'nik' => $this->input->post('nik'),
                'no_kk' => $this->input->post('no_kk'),
                'hub_dalam_kk' => $this->input->post('hub_dalam_kk'),
                'nama' => $this->input->post('nama'),
                'tempat_lahir' => $this->input->post('tempat_lahir'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                'agama' => $this->input->post('agama'),
                'is_laki' => $this->input->post('is_laki'),
                'pendidikan_dalamkk' => $this->input->post('pendidikan_dalamkk'),
                'pekerjaan' => $this->input->post('pekerjaan'),
                'status_warga_negara' => $this->input->post('status_warga_negara'),
                'nik_ayah' => $this->input->post('nik_ayah'),
                'nama_ayah' => $this->input->post('nama_ayah'),
                'nik_ibu' => $this->input->post('nik_ibu'),
                'nama_ibu' => $this->input->post('nama_ibu'),
                'id_kecamatan' => $this->input->post('id_kecamatan'),
                'id_desa' => $this->input->post('id_desa'),
                'alamat' => $this->input->post('alamat'),
                'rt' => $this->input->post('rt'),
                'rw' => $this->input->post('rw'),
                'no_telepon' => $this->input->post('no_telepon'),
                'alamat_email' => $this->input->post('alamat_email'),
                'status_perkawinan' => $this->input->post('status_perkawinan'),
                'no_akta_nikah' => $this->input->post('no_akta_nikah'),
                'golongan_darah' => $this->input->post('golongan_darah'),
                'keterangan' => $this->input->post('keterangan'),
                //  'gambar_profil' => $this->input->post(''),
                //  'gambar_ktp' => $this->input->post(''),
                //  'gambar_kk' => $this->input->post(''),
                'status_bantuan' => '1',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_by' => $this->session->userdata(SESSION_LOGIN_USERNAME),
                'created_by'=> $this->session->userdata(SESSION_LOGIN_USERNAME)
                );
                //echo json_encode($params);

                $penduduk_id = $this->Penduduk_model->add_penduduk($params);
                $this->session->set_flashdata('pesan', "Tambah Data Berhasil");
  
            }
            redirect('penduduk/index');
        }
        else
        {
            $data['setting'] = $this->Setting_model->get_setting('1');
            $data['all_desa'] = $this->Tweb_desa_model->get_all_tweb_desa();
            $data['all_kecamatan'] = $this->Tweb_kecamatan_model->get_all_tweb_kecamatan();
            $data['all_status_kawin'] = $this->Tweb_penduduk_kawin_model->get_all_tweb_penduduk_kawin();
            $data['all_pekerjaan'] = $this->Tweb_penduduk_pekerjaan_model->get_all_tweb_penduduk_pekerjaan();
            $data['all_pendidikan'] = $this->Tweb_penduduk_pendidikan_kk_model->get_all_tweb_penduduk_pendidikan_kk();
            $data['all_status_warga'] = $this->Tweb_penduduk_warganegara_model->get_all_tweb_penduduk_warganegara();
            $data['all_golongan_darah'] = $this->Tweb_golongan_darah_model->get_all_tweb_golongan_darah();
            $data['all_agama'] = $this->Tweb_penduduk_agama_model->get_all_tweb_penduduk_agama();
            $data['all_hubungan'] = $this->Tweb_penduduk_hubungan_model->get_all_tweb_penduduk_hubungan();
            $data['_view']='penduduk/add';
            $data['_header'] = 'layouts/admin_header';
            $data['_sidebar'] = 'layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
    }


    /*
     * Editing a penduduk
     */
    function edit($id)
    {   
        // check if the penduduk exists before trying to edit it
        $data['penduduk'] = $this->Penduduk_model->get_penduduk(array('penduduk.id'=>$id));
        
        if(isset($data['penduduk']['id']))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nik','No Hp','required|integer');
            $this->form_validation->set_rules('nama','Nama','required');
            $this->form_validation->set_rules('no_kk','Instansi','required');
            $this->form_validation->set_rules('alamat','Tujuan','required');
       
			if($this->form_validation->run())     
            {   
                if($this->input->post('nik')!=$this->input->post('old_nik')){
                    if($this->is_nik_registered($this->input->post('nik'))){
                        $this->session->set_flashdata('pesan', "NIK Telah Terdaftar");
                        redirect('penduduk/index');
                    }
                }

                $params = array(
                    'nik' => $this->input->post('nik'),
                    'no_kk' => $this->input->post('no_kk'),
                    'hub_dalam_kk' => $this->input->post('hub_dalam_kk'),
                    'nama' => $this->input->post('nama'),
                    'tempat_lahir' => $this->input->post('tempat_lahir'),
                    'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                    'agama' => $this->input->post('agama'),
                    'is_laki' => $this->input->post('is_laki'),
                    'pendidikan_dalamkk' => $this->input->post('pendidikan_dalamkk'),
                    'pekerjaan' => $this->input->post('pekerjaan'),
                    'status_warga_negara' => $this->input->post('status_warga_negara'),
                    'nik_ayah' => $this->input->post('nik_ayah'),
                    'nama_ayah' => $this->input->post('nama_ayah'),
                    'nik_ibu' => $this->input->post('nik_ibu'),
                    'nama_ibu' => $this->input->post('nama_ibu'),
                    'id_kecamatan' => $this->input->post('id_kecamatan'),
                    'id_desa' => $this->input->post('id_desa'),
                    'alamat' => $this->input->post('alamat'),
                    'rt' => $this->input->post('rt'),
                    'rw' => $this->input->post('rw'),
                    'no_telepon' => $this->input->post('no_telepon'),
                    'alamat_email' => $this->input->post('alamat_email'),
                    'status_perkawinan' => $this->input->post('status_perkawinan'),
                    'no_akta_nikah' => $this->input->post('no_akta_nikah'),
                    'golongan_darah' => $this->input->post('golongan_darah'),
                    'keterangan' => $this->input->post('keterangan'),
                    //  'gambar_profil' => $this->input->post(''),
                    //  'gambar_ktp' => $this->input->post(''),
                    //  'gambar_kk' => $this->input->post(''),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => $this->session->userdata(SESSION_LOGIN_USERNAME),
                );
                
                $this->Penduduk_model->update_penduduk($id,$params);   
                $this->session->set_flashdata('pesan', "Ubah data berhasi");         

                redirect('penduduk/index');
            }
            else
            {
                $data['setting'] = $this->Setting_model->get_setting('1');
                $data['all_desa'] = $this->Tweb_desa_model->get_all_tweb_desa();
                $data['all_kecamatan'] = $this->Tweb_kecamatan_model->get_all_tweb_kecamatan();
                $data['all_status_kawin'] = $this->Tweb_penduduk_kawin_model->get_all_tweb_penduduk_kawin();
                $data['all_pekerjaan'] = $this->Tweb_penduduk_pekerjaan_model->get_all_tweb_penduduk_pekerjaan();
                $data['all_pendidikan'] = $this->Tweb_penduduk_pendidikan_kk_model->get_all_tweb_penduduk_pendidikan_kk();
                $data['all_status_warga'] = $this->Tweb_penduduk_warganegara_model->get_all_tweb_penduduk_warganegara();
                $data['all_golongan_darah'] = $this->Tweb_golongan_darah_model->get_all_tweb_golongan_darah();
                $data['all_agama'] = $this->Tweb_penduduk_agama_model->get_all_tweb_penduduk_agama();
                $data['all_hubungan'] = $this->Tweb_penduduk_hubungan_model->get_all_tweb_penduduk_hubungan(); 
                $data['_header'] = 'layouts/admin_header';
                $data['_sidebar'] = 'layouts/admin_sidebar';
                $data['_view'] = 'penduduk/edit';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The penduduk you are trying to edit does not exist.');
    } 
    function ubah_status($id){
        $penduduk = $this->Penduduk_model->get_penduduk(array("penduduk.id"=> $id));
        if(isset($penduduk['id'])){
            $params=array(
                "status_bantuan" => "2",
                "updated_by" => $this->session->userdata(SESSION_LOGIN_USERNAME),
                "updated_at" => date("Y-m-d H:i:s")
            );
            $this->Penduduk_model->update_penduduk($id,$params);
            redirect('penduduk/index');
        }else{
            show_error("Penduduk tidak ditemukan");
        }
        
    }
    /*
     * Deleting penduduk
     */
    function remove($id)
    {
        
        $penduduk = $this->Penduduk_model->get_penduduk(array("penduduk.id"=>$id));

        // check if the penduduk exists before trying to delete it
        if(isset($penduduk['id']))
        {
            $this->Penduduk_model->delete_penduduk($id);
            redirect('penduduk/index');
        }
        else
            show_error('The penduduk you are trying to delete does not exist.');
    }
    function get_desa_by_idkecamatan_json(){
        $id_kecamatan=$this->input->post("id_kecamatan");
        $listdesa = $this->Tweb_desa_model->get_all_tweb_desa(array('district_id'=>$id_kecamatan));
        echo json_encode($listdesa);
    }
    function is_nik_registered($nik){
        $penduduk = $this->Penduduk_model->get_penduduk(array('nik'=>$nik));
        if($penduduk){
            return true;
        }else{
            return false;
        }
    }
    function get_data_penduduk_json()
    {
        $params_where=array();
        // $params_where=array(
        //     'tanggal >=' => $this->input->post('waktu_dari')
        // );

        if($this->input->post('id_desa')!='-1'){
            $params_where['id_desa']=$this->input->post('id_desa');
        }
        if($this->input->post('id_kecamatan')!='-1'){
            $params_where['id_kecamatan'] = $this->input->post('id_kecamatan');
        }
        
        $list = $this->Penduduk_model->get_datatables($params_where);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $field->nik;
            $row[] = $field->no_kk . "<br>" . "(". $field->nama_hubungan . ")";
            $row[] = $field->nama;
            $row[] = $field->nama_desa;
            $row[] = $field->nama_kecamatan;
            $row[] = $field->nama_status_bantuan;
            $row[] = "<a href='" .  'detail/'. $field->id ."'" . "class='btn btn-warning btn-xs'><span class='fa fa-arrow-right'></span> Detail</a>" .
            "<br>" .
            "<a href='" .  'edit/'. $field->id ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>".
            "<br>" .
            "<a href='" .  'ubah_status/'. $field->id ."'" . "class='btn btn-primary btn-xs' onclick='return confirm(\" Apakah Penduduk Ini sudah menerima bantuan ? \")'><span class='fa fa-handshake-o'></span> Terima Bantuan</a>".
            "<br>" .
            "<a href='" .  'remove/'. $field->id ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";
            $data[] = $row;
        }
        //echo $this->Penduduk_model->count_all($params_where);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Penduduk_model->count_all($params_where),
            "recordsFiltered" => $this->Penduduk_model->count_filtered($params_where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
