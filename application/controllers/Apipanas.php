<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

Class Apipanas extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Token_android_model');
        $this->load->model('Penduduk_model');
    }

    
    public function get_all_penduduk(){
        $token=$this->input->get('token');
        $check_token= $this->check_token($token);

        if($check_token){
            $all_penduduk=$this->Penduduk_model->get_all_penduduk();
            $dump= array(
                'status'=>TRUE,
                'error'=>'',
                'data'=>$all_penduduk
            );
            echo str_replace('/n','',json_encode($dump));
        }
        
    }
    public function search_penduduk(){
        $token=$this->input->post('token');
        $nik=$this->input->post('nik');
        $check_token= $this->check_token($token);

        if($check_token){
            $penduduk = $this->Penduduk_model->get_penduduk(array('nik'=>$nik));
            if($penduduk){
                $this->_return(array(
                    'status'=>TRUE,
                    'error'=>'',
                    'data'=>$penduduk
                ));
            }else{
                $this->_return(
                    array(
                        'status'=>FALSE,
                        'error'=>'Data Tidak Ditemukan',
                        'data'=>''
                    )
                );
            }
        }
        
    }
    public function penduduk_terima_bantuan(){
        $token=$this->input->post('token');
        $nik=$this->input->post('nik');
        
        $check_token= $this->check_token($token);

        if($check_token){
            $penduduk = $this->Penduduk_model->get_penduduk(array('nik'=>$nik));
            $token_android=$this->Token_android_model->get_token_android(array(
                'token'=>$token
            ));
            if($penduduk){
                $update=$this->Penduduk_model->update_penduduk($penduduk['id'],array(
                    'status_bantuan' => '2',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => $token_android['user_id']
                ));
                $this->_return(array(
                    'status'=>TRUE,
                    'error'=>'',
                    'data'=>$update
                ));
            }else{
                $this->_return(array(
                    'status'=>FALSE,
                    'error'=>'Data Tidak Ditemukan',
                    'data'=>''
                ));
            }
        }
        
    }

    //KREDENSIAL
    public function is_token_active(){
        $token= $this->input->post('token');
        $check_token = $this->check_token($token);
        if($check_token){
            $this->_return(array(
                'status'=>TRUE,
                'error'=>'',
                'data'=>'Token Aktif'
            ));
        }
    }
    public function login(){
        $username = $this->input->post('user');
        $pass = $this->input->post('password');
        $user = $this->User_model->get_user($username);
		//print_r($user);
		if($user){
			if(password_verify($pass, $user['pass']) && $user['ustate']=='1') {
                $find_token=$this->find_token( array('user_id' =>$user['userid']));
                if(empty($find_token)){
                    $token=md5(date('YmdHis') . SECRET_KEY);
                    $this->add_token($token,$user['userid']);
                    $user['token'] = $token;     
                    
                }else{
                    $user['token'] = $find_token['token'];     
                }

                $this->_return(array(
                    'status'=>TRUE,
                    'error'=>'',
                    'data'=>$user
                ));

                
			}else{
                $this->_return(array(
                    'status'=>FALSE,
                    'error'=>'Username tidak ditemukan',
                    'data'=>''
                ));
            }
		}else{
            $this->_return(array(
                'status'=>FALSE,
                'error'=>'Username tidak ditemukan',
                'data'=>''
            ));
        }

    }

    function check_token($token){
        $token_android=$this->Token_android_model->get_token_android(array(
            'token'=>$token
        ));
        $now=date('Y-m-d H:i:s');

        if($token_android){
            if($now <= $token_android['expired_at']){
                return true;
            }else{
                $this->delete_token($token_android['token'],$token_android['user_id']);
                $this->_return(
                    array(
                        'status'=>FALSE,
                        'error'=>'Token Kadaluwarsa',
                        'data'=>''
                    )
                );
                return false;
            }
        }else{
            $this->_return(
                array(
                    'status'=>FALSE,
                    'error'=>'Token Tidak Valid',
                    'data'=>''
                )
            );
            return false;
        }
    }
    
    //PENUNJANG
    function _return($message=array()){
        echo str_replace('/n','',json_encode($message));
    }
    function add_token($token,$userid){
        $this->Token_android_model->add_token_android(array(
            'token'=> $token,	
            'user_id' => $userid,
            'created_at'=> date('Y-m-d H:i:s'),
            'expired_at'=> date('Y-m-d H:i:s',strtotime('+'. LIFETIME_TOKEN.' minutes')),
        ));
    }
    function delete_token($token,$userid){
        $this->Token_android_model->delete_token_android(array(
            'token' => $token,
            'user_id'=>$userid
        ));
    }
    function find_token($array){        
        $token_android=$this->Token_android_model->get_token_android($array);
        $now=date('Y-m-d H:i:s');

        if($token_android){
            if($now <= $token_android['expired_at']){
                return $token_android;
            }else{
                $this->delete_token($token_android['token'],$token_android['user_id']);
            }
        }
        return array();
    }
}

?>