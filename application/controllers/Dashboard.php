<?php

class Dashboard extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penduduk_model');
        $this->load->model('Tweb_kecamatan_model');
        $this->load->model('Tweb_desa_model');
        $this->load->model('Tweb_penduduk_pekerjaan_model');
        $this->load->model('Tweb_penduduk_pendidikan_kk_model');
    }
    function get_desa_by_idkecamatan_json(){
        $id_kecamatan=$this->input->post("id_kecamatan");
        $listdesa = $this->Tweb_desa_model->get_all_tweb_desa(array('district_id'=>$id_kecamatan));
        echo json_encode($listdesa);
    }
    
    function index()
    {
        $params_where=array();
    	if(isset($_POST) && count($_POST) > 0){   
            if($this->input->post('id_kecamatan')!='-1'){
                $params_where['id_kecamatan']=$this->input->post('id_kecamatan');
            }

            if($this->input->post('id_desa')!='-1'){
                $params_where['id_desa']=$this->input->post('id_desa');
            }
        }
        // //**************************ALGORITMA UNTUK GRAFIK PENDIDIKAN**********************
        $arrayresult=array();
        $datagrafik_mentah=$this->Penduduk_model->get_all_penduduk_groupby('count(penduduk.id) as jumlah_data,pendidikan_dalamkk',
            $params_where,
            "pendidikan_dalamkk");
        foreach($datagrafik_mentah as $datagrafik){
            $pendidikan = $this->Tweb_penduduk_pendidikan_kk_model->get_tweb_penduduk_pendidikan_kk($datagrafik['pendidikan_dalamkk']);
            $array_temp=array(
                'name' => $pendidikan['nama'],
                'y' => (int)$datagrafik['jumlah_data']
            );
            array_push($arrayresult,$array_temp);
        }
        $data['data_grafik_pendidikan']=$arrayresult;
        // //**************************ALGORITMA UNTUK GRAFIK PEKERJAAN**********************
        $arrayresult=array();
        $datagrafik_mentah=$this->Penduduk_model->get_all_penduduk_groupby('count(penduduk.id) as jumlah_data,pekerjaan',
            $params_where,
            "pekerjaan");
        foreach($datagrafik_mentah as $datagrafik){
            $pekerjaan = $this->Tweb_penduduk_pekerjaan_model->get_tweb_penduduk_pekerjaan($datagrafik['pekerjaan']);
            $array_temp=array(
                'name' => $pekerjaan['nama'],
                'y' => (int)$datagrafik['jumlah_data']
            );
            array_push($arrayresult,$array_temp);
        }
        $data['data_grafik_pekerjaan']=$arrayresult;
        $data['all_desa'] =$this->Tweb_desa_model->get_all_tweb_desa();
        $data['all_kecamatan'] =$this->Tweb_kecamatan_model->get_all_tweb_kecamatan();
        $data['_view'] = 'dashboard/dashboard';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar']='layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);    

    }
}
