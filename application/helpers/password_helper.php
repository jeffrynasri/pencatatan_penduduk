<?php if(!defined('BASEPATH'))exit("No Direct script access allowed");

//ENKRIP
if(!function_exists('hash_password')){
  function hash_password($pass){
    $options=[
      'cost' => COST_ENCRYPT_PASSWORD
    ];
    return password_hash($pass, PASSWORD_BCRYPT, $options);
  }
}
