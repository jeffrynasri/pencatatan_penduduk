<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Ubah pengaturan</h3>
				<!-- <p><span class="text-danger">*</span>Untuk Ubah Pengaturan Hubungi Dinas Kominfo</p> -->
            </div>
			<?php echo form_open('setting/index'); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-12">
						<label for="pengumuman" class="control-label"><span class="text-danger">*</span>Pengumuman</label>
						<div class="form-group">
							<textarea name="pengumuman" class="form-control" id="pengumuman"><?php echo ($this->input->post('pengumuman') ? $this->input->post('pengumuman') : $setting['pengumuman']); ?></textarea>
							<span class="text-danger"><?php echo form_error('pengumuman');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="is_popup_active" class="control-label"><span class="text-danger">*</span>Pop Up Tampil</label>
						<div class="form-group">
							<select name="is_popup_active" id='is_popup_active' class="form-control">
								<option <?php echo $setting['is_popup_active']==0?'selected':'' ?> value='0'>Tidak Aktif</option>
								<option <?php echo $setting['is_popup_active']==1?'selected':'' ?> value='1'>Aktif</option>
							</select>
							<span class="text-danger"><?php echo form_error('is_popup_active');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="is_camera_active" class="control-label"><span class="text-danger">*</span>Pendaftaran</label>
						<div class="form-group">
							<select name="is_camera_active" id='is_camera_active' class="form-control">
								<option <?php echo $setting['is_camera_active']==0?'selected':'' ?> value='0'>Tutup</option>
								<option <?php echo $setting['is_camera_active']==1?'selected':'' ?> value='1'>Dibuka</option>
							</select>
							<span class="text-danger"><?php echo form_error('is_camera_active');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="password_dewa" class="control-label"><span class="text-danger">*</span>Password</label>
						<div class="form-group">
							<input type="password" name="password_dewa" class="form-control" id="password_dewa" />
							<span class="text-danger"><?php echo form_error('password_dewa');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Simpan
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>