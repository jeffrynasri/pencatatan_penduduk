<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Detail Keluarga dengan no KK : <?php echo $no_kk; ?></h3>
            </div>

			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-12">
						<table class='table table-hover'>
							<tbody>
								<tr>
									<th>No KK</th>
									<th>NIK</th>
									<th>Nama</th>
									<th>Hubungan dalam KK</th>
									<th>Pekerjaan</th>
									<th>Pendidikan</th>
								</tr>
								<?php foreach($keluarga as $kl){ ?>
									<tr>
										<td><?php echo $kl['no_kk']; ?></td>
										<td><?php echo $kl['nik']; ?></td>
										<td><?php echo $kl['nama']; ?></td>
										<td><?php echo $kl['nama_hubungan']; ?></td>
										<td><?php echo $kl['nama_pekerjaan']; ?></td>
										<td><?php echo $kl['nama_pendidikan']; ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>			
		</div>
    </div>
</div>