<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Keluarga</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="box-body">
                        <div class='row'>
                            <div class='col-md-12'>
                                <span>Kecamatan</span>
                                <select name='id_kecamatan' id='id_kecamatan'>
                                    <option value='-1' selected>Semua</option>
                                    <?php foreach($all_kecamatan as $kecamatan){ ?>
                                        <option value='<?php echo $kecamatan['id']; ?>'><?php echo $kecamatan['nama']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class='col-md-12'>
                                <span>Desa</span>
                                <select name='id_desa' id='id_desa'>
                                    <option value='-1' selected>Semua</option>
                                    <?php foreach($all_desa as $desa){ ?>
                                        <option value="<?php echo $desa['id']; ?>"><?php echo $desa['nama']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <div class='col-md-12'>
                                <button id='filter' class='btn btn-primary' >Pilih</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" >
                        <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                           <thead>
                                <tr>
                                    <th>No KK</th>
                                    <th>Jumlah anggota keluarga</th>
                                    <!-- <th>Desa</th>
                                    <th>Kecamatan</th> -->
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#custom_datatable').DataTable({ 
            dom: 'lBfrtip',
            buttons:[
                'copy','csv','excel','print',{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize:'A3',
                }
            ],
            iDisplayLength: 25,
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                "url": "<?php echo site_url('keluarga/get_data_keluarga_json')?>",
                'data':function(d){
                    d.id_desa = $('#id_desa option:selected').val(),
                    d.id_kecamatan = $('#id_kecamatan option:selected').val()                    
                },
                "type": "POST",
                'dataSrc': function(json){
                    console.log(json.data);
                    return json.data
                }
                
            },
            
            "columnDefs": [
                { 
                    "targets": [ 2], 
                    "orderable": false, 
                },
                {'targets':0, 'width': '40%'},
                {'targets':1, 'width': '40%'},
                {'targets':2, 'width': '20%'},           
            ],
 
        });
        $('#filter').click(function(){
            console.log("hura");
            $('#custom_datatable').DataTable().ajax.reload();
        })
        //ALGO UNTUK FILTER KECAMATAN
        $('#id_kecamatan').on('change',function(){
            console.log('berubah')
            $.ajax({
                url: "<?php echo site_url('keluarga/get_desa_by_idkecamatan_json'); ?>",
                type: 'POST',
                dataType: 'json',
                data: {
                    'id_kecamatan':$('#id_kecamatan option:selected').val()
                },
                success: function(data){
                    console.log(data)
                    $('#id_desa').empty()
                    $('#id_desa').append('<option value="-1">Semua</option>')
                    for(var desa of data){
                        $('#id_desa').append("<option value='"+desa.id+ "'>"+ desa.nama +"</option>")
                        
                    }
                }
            })

        });
        
        // $('#custom_datatable').DataTable().ajax.reload();
    }); 
 
</script>