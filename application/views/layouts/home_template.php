<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title><?php echo APP_NAME; ?></title>
	<!-- ICON -->
	<link rel='icon' href="<?php echo site_url('resources/images/gresikkab_logo.png');?>"/>
  <style>
     body {
         display: flex;
         min-height: 100vh;
         flex-direction: column;
     }
     main {
         flex: 1 0 auto;
     }
  </style>
  
  <!-- Webcam -->
  <script src="<?php echo base_url(); ?>resources/js/webcam.js"></script>

	<!-- Jquery 3.3.1 -->
	<script src="<?php echo base_url();?>resources/js/jquery-3.3.1.js" ></script>
  
	<!-- Materialize -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>resources/css/materialize.min.css"  media="screen,projection" defer/>
	<script src="<?php echo base_url();?>resources/js/materialize.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons&display=swap" rel="stylesheet" defer>

</head>
<body>
  <?php $this->load->view('layouts/home_header'); ?>
  <main>
    <?php
    if(isset($_view) && $_view) $this->load->view($_view);
    ?>
  </main>
  <?php $this->load->view('layouts/home_footer'); ?>
</body>

</html>
