<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <!-- <li class="header">Kriteria</li> -->
        <li class="treeview active">
            <a href="#">
                <i class="fa fa-users"></i> <span>Penduduk</span>
            </a>
            <ul class="treeview-menu">
                  <li class="<?php if($this->uri->segment(1,0)=='dashboard'){echo 'active';}else{echo '';}?>">
                    <a href="<?php echo site_url('dashboard/index') ?>"><i class="fa fa-area-chart"></i><span>Dashboard</span></a>
                  </li>
                  <li class="<?php if($this->uri->segment(1,0)=='penduduk'){echo 'active';}else{echo '';}?>">
                    <a href="<?php echo site_url('penduduk/index') ?>"><i class="fa fa-group"></i><span>Perorangan</span></a>
                  </li>
                  <li class="<?php if($this->uri->segment(1,0)=='keluarga'){echo 'active';}else{echo '';}?>">
                    <a href="<?php echo site_url('keluarga/index') ?>"><i class="fa fa-group"></i><span>Keluarga</span></a>
                  </li>
            </ul>
        </li>
        <!-- <li class="header">Pengaturan</li>
        <li class="<?php if($this->uri->segment(1,0)=='tujuan'){echo 'active';}else{echo '';}?>">
          <a href="<?php echo site_url('tujuan/index') ?>"><i class="fa fa-location-arrow"></i><span>Tujuan</span></a>
        </li>
        <li class="<?php if($this->uri->segment(1,0)=='layanan'){echo 'active';}else{echo '';}?>">
          <a href="<?php echo site_url('layanan/index') ?>"><i class="fa fa-hand-o-right"></i><span>Layanan</span></a>
        </li>
        <li class="<?php if($this->uri->segment(1,0)=='aspek_skm'){echo 'active';}else{echo '';}?>">
          <a href="<?php echo site_url('aspek_skm/index') ?>"><i class="fa fa-sticky-note"></i><span>Penilaian</span></a>
        </li> -->
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
