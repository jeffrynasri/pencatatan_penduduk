<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Detail Penduduk</h3>
            </div>

			<?php echo form_open('penduduk/edit/'.$penduduk['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-12">
						<label for="id_kecamatan" class="control-label">Kecamatan</label>
						<div class="form-group">
							<select disabled name='id_kecamatan' id='id_kecamatan' class='form-control'>
								<?php foreach($all_kecamatan as $kecamatan){ ?>
									<option value="<?php echo $kecamatan['id']; ?>" <?php echo ($kecamatan['id']==$penduduk['id_kecamatan'])?'selected':''; ?> ><?php echo $kecamatan['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('id_kecamatan');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="id_desa" class="control-label">Desa</label>
						<div class="form-group">
							<select disabled name='id_desa' id='id_desa' class='form-control'>
								<?php foreach($all_desa as $desa){ ?>
									<option value="<?php echo $desa['id']; ?>" <?php echo ($desa['id']==$penduduk['id_desa'])?'selected':''; ?> ><?php echo $desa['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('id_desa');?></span>
						</div>
					</div>

					<div class="col-md-12">
						<label for="nik" class="control-label"><span class="text-danger">*</span>NIK</label>
						<div class="form-group">
							<input disabled type="number" name="nik" value="<?php echo ($this->input->post('nik') ? $this->input->post('nik') : $penduduk['nik']); ?>" class="form-control" id="nik" />
							<span class="text-danger"><?php echo form_error('nik');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="no_kk" class="control-label"><span class="text-danger">*</span>No KK</label>
						<div class="form-group">
							<input disabled type="number" name="no_kk" value="<?php echo ($this->input->post('no_kk') ? $this->input->post('no_kk') : $penduduk['no_kk']); ?>" class="form-control" id="no_kk" />
							<span class="text-danger"><?php echo form_error('no_kk');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="hub_dalam_kk" class="control-label"><span class="text-danger">*</span>Hubungan dalam KK</label>
						<div class="form-group">
							<select disabled name='hub_dalam_kk' id='hub_dalam_kk' class='form-control'>
								<?php foreach($all_hubungan as $hubungan){ ?>
									<option value="<?php echo $hubungan['id']; ?>" <?php echo ($hubungan['id']==$penduduk['hub_dalam_kk'])?'selected':''; ?> ><?php echo $hubungan['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('hub_dalam_kk');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="nama" class="control-label"><span class="text-danger">*</span>Nama</label>
						<div class="form-group">
							<input disabled type="text" name="nama" value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : $penduduk['nama']); ?>" class="form-control" id="nama" />
							<span class="text-danger"><?php echo form_error('nama');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="alamat" class="control-label"><span class='text-danger'>*</span>Alamat</label>
						<div class="form-group">
							<input disabled type="text" name="alamat" value="<?php echo ($this->input->post('alamat') ? $this->input->post('alamat') : $penduduk['alamat']); ?>" class="form-control" id="alamat" />
							<span class="text-danger"><?php echo form_error('alamat');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="rt" class="control-label">RT</label>
						<div class="form-group">
							<input disabled type="number" name="rt" value="<?php echo ($this->input->post('rt') ? $this->input->post('rt') : $penduduk['rt']); ?>" class="form-control" id="rt" />
							<span class="text-danger"><?php echo form_error('rt');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="rw" class="control-label"><span class="text-danger">*</span>rw</label>
						<div class="form-group">
							<input disabled type="number" name="rw" value="<?php echo ($this->input->post('rw') ? $this->input->post('rw') : $penduduk['rw']); ?>" class="form-control" id="rw" />
							<span class="text-danger"><?php echo form_error('rw');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="tempat_lahir" class="control-label">Tempat lahir</label>
						<div class="form-group">
							<input disabled type="text" name="tempat_lahir" value="<?php echo ($this->input->post('tempat_lahir') ? $this->input->post('tempat_lahir') : $penduduk['tempat_lahir']); ?>" class="form-control" id="tempat_lahir" />
							<span class="text-danger"><?php echo form_error('tempat_lahir');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="tanggal_lahir" class="control-label">Tanggal lahir</label>
						<div class="form-group">
							<input disabled type="datetime-local" name="tanggal_lahir" value="<?php echo ($this->input->post('tanggal_lahir') ? $this->input->post('tanggal_lahir') : date('Y-m-d\TH:i',strtotime($penduduk['tanggal_lahir']))); ?>" class="form-control" id="tanggal_lahir" />
							<span class="text-danger"><?php echo form_error('tanggal_lahir');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="agama" class="control-label">Agama</label>
						<div class="form-group">
							<input disabled type="text" name="agama" value="<?php echo ($this->input->post('agama') ? $this->input->post('agama') : $penduduk['agama']); ?>" class="form-control" id="agama" />
							<span class="text-danger"><?php echo form_error('agama');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="is_laki" class="control-label">Jenis Kelamin</label>
						<div class="form-group">
							<select disabled name='is_laki' id='is_laki' class='form-control'>
								<option value="1" <?php echo ($penduduk['is_laki']=='1')?'selected':""; ?>>Laki-Laki</option>
								<option value="0" <?php echo ($penduduk['is_laki']=='0')?'selected':""; ?>>Perempuan</option>
							</select>
							<span class="text-danger"><?php echo form_error('is_laki');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="pendidikan_dalamkk" class="control-label">Pendidikan</label>
						<div class="form-group">
							<select disabled name='pendidikan_dalamkk' id='pendidikan_dalamkk' class='form-control'>
								<?php foreach($all_pendidikan as $pendidikan){ ?>
									<option value="<?php echo $pendidikan['id']; ?>" <?php echo ($pendidikan['id']==$penduduk['pendidikan_dalamkk'])?'selected':''; ?> ><?php echo $pendidikan['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('pendidikan_dalamkk');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="pekerjaan" class="control-label">Pekerjaan</label>
						<div class="form-group">
							<select disabled name='pekerjaan' id='pekerjaan' class='form-control'>
								<?php foreach($all_pekerjaan as $pekerjaan){ ?>
									<option value="<?php echo $pekerjaan['id']; ?>" <?php echo ($pekerjaan['id']==$penduduk['pekerjaan'])?'selected':''; ?> ><?php echo $pekerjaan['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('pekerjaan');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="status_warga_negara" class="control-label">Status Kewarganegaraan</label>
						<div class="form-group">
							<select disabled name='status_warga_negara' id='status_warga_negara' class='form-control'>
								<?php foreach($all_status_warga as $status_warga){ ?>
									<option value="<?php echo $status_warga['id']; ?>" <?php echo ($status_warga['id']==$penduduk['status_warga_negara'])?'selected':''; ?> ><?php echo $status_warga['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('status_warga_negara');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="nik_ayah" class="control-label">NIK ayah</label>
						<div class="form-group">
							<input disabled type="number" name="nik_ayah" value="<?php echo ($this->input->post('nik_ayah') ? $this->input->post('nik_ayah') : $penduduk['nik_ayah']); ?>" class="form-control" id="nik_ayah" />
							<span class="text-danger"><?php echo form_error('nik_ayah');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="nama_ayah" class="control-label">Nama ayah</label>
						<div class="form-group">
							<input disabled type="text" name="nama_ayah" value="<?php echo ($this->input->post('nama_ayah') ? $this->input->post('nama_ayah') : $penduduk['nama_ayah']); ?>" class="form-control" id="nama_ayah" />
							<span class="text-danger"><?php echo form_error('nama_ayah');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="nik_ibu" class="control-label">NIK Ibu</label>
						<div class="form-group">
							<input disabled type="number" name="nik_ibu" value="<?php echo ($this->input->post('nik_ibu') ? $this->input->post('nik_ibu') : $penduduk['nik_ibu']); ?>" class="form-control" id="nik_ibu" />
							<span class="text-danger"><?php echo form_error('nik_ibu');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="nama_ibu" class="control-label">Nama Ibu</label>
						<div class="form-group">
							<input disabled type="text" name="nama_ibu" value="<?php echo ($this->input->post('nama_ibu') ? $this->input->post('nama_ibu') : $penduduk['nama_ibu']); ?>" class="form-control" id="nama_ibu" />
							<span class="text-danger"><?php echo form_error('nama_ibu');?></span>
						</div>
					</div>
					
					
					<div class="col-md-12">
						<label for="no_telepon" class="control-label"><span class="text-danger">*</span>No Telepon</label>
						<div class="form-group">
							<input disabled type="number" name="no_telepon" value="<?php echo ($this->input->post('no_telepon') ? $this->input->post('no_telepon') : $penduduk['no_telepon']); ?>" class="form-control" id="no_telepon" />
							<span class="text-danger"><?php echo form_error('no_telepon');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="alamat_email" class="control-label">Alamat Email</label>
						<div class="form-group">
							<input disabled type="text" name="alamat_email" value="<?php echo ($this->input->post('alamat_email') ? $this->input->post('alamat_email') : $penduduk['alamat_email']); ?>" class="form-control" id="alamat_email" />
							<span class="text-danger"><?php echo form_error('alamat_email');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="status_perkawinan" class="control-label">Status Perkawinan</label>
						<div class="form-group">
							<select disabled name='status_perkawinan' id='status_perkawinan' class='form-control'>
								<?php foreach($all_status_kawin as $status_perkawinan){ ?>
									<option value="<?php echo $status_perkawinan['id']; ?>" <?php echo ($status_perkawinan['id']==$penduduk['status_perkawinan'])?'selected':''; ?> ><?php echo $status_perkawinan['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('status_perkawinan');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="no_akta_nikah" class="control-label">No Akta Nikah</label>
						<div class="form-group">
							<input disabled type="text" name="no_akta_nikah" value="<?php echo ($this->input->post('no_akta_nikah') ? $this->input->post('no_akta_nikah') : $penduduk['no_akta_nikah']); ?>" class="form-control" id="no_akta_nikah" />
							<span class="text-danger"><?php echo form_error('no_akta_nikah');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="golongan_darah" class="control-label">Golongan Darah</label>
						<div class="form-group">
							<select disabled name='golongan_darah' id='golongan_darah' class='form-control'>
								<?php foreach($all_golongan_darah as $golongan_darah){ ?>
									<option value="<?php echo $golongan_darah['id']; ?>" <?php echo ($golongan_darah['id']==$penduduk['golongan_darah'])?'selected':''; ?> ><?php echo $golongan_darah['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('golongan_darah');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="status_bantuan" class="control-label">Status Bantuan</label>
						<div class="form-group">
							<select disabled name='status_bantuan' id='status_bantuan' class='form-control'>
								<?php foreach($all_status_bantuan as $status_bantuan){ ?>
									<option value="<?php echo $status_bantuan['id']; ?>" <?php echo ($status_bantuan['id']==$penduduk['status_bantuan'])?'selected':''; ?> ><?php echo $status_bantuan['nama']; ?></option>
								<?php } ?>
							</select>
							<span class="text-danger"><?php echo form_error('status_bantuan');?></span>
						</div>
					</div>
					
					<div class="col-md-12">
						<label for="keterangan" class="control-label">Keterangan</label>
						<div class="form-group">
							<textarea disabled rows="4" value="" type="text" name="keterangan" class="form-control" id="keterangan"><?php echo ($this->input->post('keterangan') ? $this->input->post('keterangan') : $penduduk['keterangan']); ?></textarea>
							<span class="text-danger"><?php echo form_error('keterangan');?></span>
						</div>
					</div>
				</div>
			</div>			
			<?php echo form_close(); ?>
		</div>
    </div>
</div>