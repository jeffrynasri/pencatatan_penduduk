<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Penduduk </h3>
      </div>
      <?php echo form_open_multipart('penduduk/add'); ?>
      <div class="box-body">
        <div class="row clearfix">
         <div class="col-md-12">
            <label for="id_kecamatan" class="control-label">Kecamatan</label>
            <div class="form-group">
              <select name='id_kecamatan' id='id_kecamatan' class='form-control'>
                <?php foreach($all_kecamatan as $kecamatan){ ?>
                  <option value="<?php echo $kecamatan['id']; ?>"><?php echo $kecamatan['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('id_kecamatan'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="id_desa" class="control-label">Desa</label>
            <div class="form-group">
              <select name='id_desa' id='id_desa' class='form-control'>
                <?php foreach($all_desa as $desa){ ?>
                  <option value="<?php echo $desa['id']; ?>"><?php echo $desa['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('id_desa'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="nik" class="control-label"><span class='text-danger'>*</span>NIK</label>
            <div class="form-group">
              <input value="" type="number" name="nik" value="<?php echo $this->input->post('nik'); ?>" class="form-control" id="nik" />
              <span class="text-danger"><?php echo form_error('nik'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="no_kk" class="control-label"><span class='text-danger'>*</span>No KK</label>
            <div class="form-group">
              <input value="" type="number" name="no_kk" value="<?php echo $this->input->post('no_kk'); ?>" class="form-control" id="no_kk" />
              <span class="text-danger"><?php echo form_error('no_kk'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="hub_dalam_kk" class="control-label">Hubungan dalam KK</label>
            <div class="form-group">
              <select name='hub_dalam_kk' id='hub_dalam_kk' class='form-control'>
                <?php foreach($all_hubungan as $hubungan){ ?>
                  <option value="<?php echo $hubungan['id']; ?>"><?php echo $hubungan['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('hub_dalam_kk'); ?></span>
            </div>
          </div>
          
          <div class="col-md-12">
            <label for="nama" class="control-label"><span class='text-danger'>*</span>Nama</label>
            <div class="form-group">
              <input value="" type="text" name="nama" value="<?php echo $this->input->post('nama'); ?>" class="form-control" id="nama" />
              <span class="text-danger"><?php echo form_error('nama'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="alamat" class="control-label"><span class='text-danger'>*</span>Alamat</label>
            <div class="form-group">
              <input value="" type="text" name="alamat" value="<?php echo $this->input->post('alamat'); ?>" class="form-control" id="alamat" />
              <span class="text-danger"><?php echo form_error('alamat'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="rt" class="control-label">RT</label>
            <div class="form-group">
              <input value="" type="number" name="rt" value="<?php echo $this->input->post('rt'); ?>" class="form-control" id="rt" />
              <span class="text-danger"><?php echo form_error('rt'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="rw" class="control-label">RW</label>
            <div class="form-group">
              <input value="" type="number" name="rw" value="<?php echo $this->input->post('rw'); ?>" class="form-control" id="rw" />
              <span class="text-danger"><?php echo form_error('rw'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="tempat_lahir" class="control-label">Tempat lahir</label>
            <div class="form-group">
              <input value="" type="text" name="tempat_lahir" value="<?php echo $this->input->post('tempat_lahir'); ?>" class="form-control" id="tempat_lahir" />
              <span class="text-danger"><?php echo form_error('tempat_lahir'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="tanggal_lahir" class="control-label">Tanggal Lahir</label>
            <div class="form-group">
              <input type="datetime-local" name="tanggal_lahir" value="<?php echo $this->input->post('tanggal_lahir')?$this->input->post('tanggal_lahir'):date('Y-m-d\TH:i') ; ?>" class="form-control" id="tanggal_lahir">
              <span class="text-danger"><?php echo form_error('tanggal_lahir'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="agama" class="control-label">Agama</label>
            <div class="form-group">
              <select name='agama' id='agama' class='form-control'>
                <?php foreach($all_agama as $agama){ ?>
                  <option value="<?php echo $agama['id']; ?>"><?php echo $agama['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('agama'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="is_laki" class="control-label">Jenis Kelamin</label>
            <div class="form-group">
              <select name='is_laki' id='is_laki' class='form-control'>
                <option value="1">Laki-Laki</option>
                <option value="0">Perempuan</option>
              </select>
              <span class="text-danger"><?php echo form_error('is_laki'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="pendidikan_dalamkk " class="control-label">Pendidikan</label>
            <div class="form-group">
              <select name='pendidikan_dalamkk' id='pendidikan_dalamkk' class='form-control'>
                <?php foreach($all_pendidikan as $pendidikan){ ?>
                  <option value="<?php echo $pendidikan['id']; ?>"><?php echo $pendidikan['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('pendidikan_dalamkk '); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="pekerjaan" class="control-label">Pekerjaan</label>
            <div class="form-group">
              <select name='pekerjaan' id='pekerjaan' class='form-control'>
                <?php foreach($all_pekerjaan as $pekerjaan){ ?>
                  <option value="<?php echo $pekerjaan['id']; ?>"><?php echo $pekerjaan['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('pekerjaan'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="status_warga_negara " class="control-label">Status Kewarganegaraan</label>
            <div class="form-group">
              <select name='status_warga_negara' id='status_warga_negara' class='form-control'>
                <?php foreach($all_status_warga as $status_warga){ ?>
                  <option value="<?php echo $status_warga['id']; ?>"><?php echo $status_warga['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('status_warga_negara '); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="nik_ayah" class="control-label">NIK ayah</label>
            <div class="form-group">
              <input value="" type="number" name="nik_ayah" value="<?php echo $this->input->post('nik_ayah'); ?>" class="form-control" id="nik_ayah" />
              <span class="text-danger"><?php echo form_error('nik_ayah'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="nama_ayah" class="control-label">Nama ayah</label>
            <div class="form-group">
              <input value="" type="text" name="nama_ayah" value="<?php echo $this->input->post('nama_ayah'); ?>" class="form-control" id="nama_ayah" />
              <span class="text-danger"><?php echo form_error('nama_ayah'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="nik_ibu" class="control-label">NIK Ibu</label>
            <div class="form-group">
              <input value="" type="number" name="nik_ibu" value="<?php echo $this->input->post('nik_ibu'); ?>" class="form-control" id="nik_ibu" />
              <span class="text-danger"><?php echo form_error('nik_ibu'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="nama_ibu" class="control-label">Nama Ibu</label>
            <div class="form-group">
              <input value="" type="text" name="nama_ibu" value="<?php echo $this->input->post('nama_ibu'); ?>" class="form-control" id="nama_ibu" />
              <span class="text-danger"><?php echo form_error('nama_ibu'); ?></span>
            </div>
          </div>
          
          <div class="col-md-12">
            <label for="no_telepon" class="control-label"><span class='text-danger'>*</span>No Telepon</label>
            <div class="form-group">
              <input value="" type="number" name="no_telepon" value="<?php echo $this->input->post('no_telepon'); ?>" class="form-control" id="no_telepon" />
              <span class="text-danger"><?php echo form_error('no_telepon'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="alamat_email" class="control-label">Alamat Email</label>
            <div class="form-group">
              <input value="" type="text" name="alamat_email" value="<?php echo $this->input->post('alamat_email'); ?>" class="form-control" id="alamat_email" />
              <span class="text-danger"><?php echo form_error('alamat_email'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="status_perkawinan" class="control-label">Status Perkawinan</label>
            <div class="form-group">
              <select name='status_perkawinan' id='status_perkawinan' class='form-control'>
                <?php foreach($all_status_kawin as $status_perkawinan){ ?>
                  <option value="<?php echo $status_perkawinan['id']; ?>"><?php echo $status_perkawinan['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('status_perkawinan'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="no_akta_nikah" class="control-label">No Akta Nikah</label>
            <div class="form-group">
              <input value="" type="text" name="no_akta_nikah" value="<?php echo $this->input->post('no_akta_nikah'); ?>" class="form-control" id="no_akta_nikah" />
              <span class="text-danger"><?php echo form_error('no_akta_nikah'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="golongan_darah" class="control-label">Golongan Darah</label>
            <div class="form-group">
              <select name='golongan_darah' id='golongan_darah' class='form-control'>
                <?php foreach($all_golongan_darah as $golongan_darah){ ?>
                  <option value="<?php echo $golongan_darah['id']; ?>"><?php echo $golongan_darah['nama']; ?></option>
                <?php } ?>
              </select>
              <span class="text-danger"><?php echo form_error('golongan_darah'); ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <label for="keterangan" class="control-label">Keterangan</label>
            <div class="form-group">
              <textarea rows="4" value="" type="text" name="keterangan" class="form-control" id="keterangan"><?php echo $this->input->post('keterangan'); ?></textarea>
              <span class="text-danger"><?php echo form_error('keterangan'); ?></span>
            </div>
          </div>
       
          <?php //for($i=1;$i <= MAX_UPLOAD_BERKASPENDUKUNG;$i++){ ?>
            <!-- <div class="col-md-12">
              <label for="<?php echo "berkaspendukung".$i;?>" class="control-label">Bukti Pendukung . <?php echo strval($i); ?></label>
              <small> jpg/jpeg/png/pdf/doc/xls : 4 Mb</small>
              <div class="form-group">
                <input multiple="multiple" type="file" name="<?php echo "berkaspendukung".$i;?>" value="<?php echo $this->input->post('<?php echo "berkaspendukung".$i;?>'); ?>" class="form-control" id="<?php echo "berkaspendukung".$i;?>" />
                <input type='text' name='<?php echo "namafile".$i; ?>' id=''<?php echo "namafile".$i; ?> class='form-control' placeholder="Nama File (Kosongkan Jika Sama)"/>
              </div>
            </div> -->
          <?php //} ?> 
        </div>
      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-success">
          <i class="fa fa-check"></i> Simpan
        </button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<script>
  // $(document).ready(function() {
    $.ajax({
      url:'<?php echo site_url("penduduk/get_desa_by_idkecamatan_json"); ?>',
      type:'post',
      dataType: 'json',
      data: {
        'id_kecamatan': $('#id_kecamatan option:selected').val()
      },
      success: function(data){
        $('#id_desa').empty()
        for(var desa of data){
          console.log(desa.nama)
          $('#id_desa').append('<option value="'+desa.id+'">'+desa.nama+'</option>')
        }
      }
    })
    $('#id_kecamatan').on("change",function(){
      $.ajax({
        url:'<?php echo site_url("penduduk/get_desa_by_idkecamatan_json"); ?>',
        type:'post',
        dataType: 'json',
        data: {
          'id_kecamatan': $('#id_kecamatan option:selected').val()
        },
        success: function(data){
          $('#id_desa').empty()
          for(var desa of data){
            console.log(desa.nama)
            $('#id_desa').append('<option value="'+desa.id+'">'+desa.nama+'</option>')
          }
        }
      })
    });
  // )};
</script>
