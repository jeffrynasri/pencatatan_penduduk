<div class="box-body">
    <div class="col-md-12">
        <div class="box" style="padding: 15px">
            <div class="box-header">
                <h3 class="box-title">Dashboard Survey Kepuasaan Pengunjung</h3>
            </div>
                <div class="row">
                    <div class='col-md-12'>
                        <?php echo form_open('dashboard_skm/index'); ?>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                <select class='form-control' name='id_tujuan' id='id_tujuan'>
                                    <option value='-1'>Semua</option>
                                    <?php foreach($all_tujuan as $tujuan){ ?>
                                        <option value='<?php echo $tujuan['id']; ?>' <?php echo ($this->input->post('id_tujuan')&&$this->input->post('id_tujuan')==$tujuan['id'])?'selected':''; ?> ><?php echo $tujuan['nama']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <button class='btn btn-primary' type='submit'>Pilih</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                	<div class="col-lg-4 col-xs-6">
                		<div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>
                              <?php echo isset($jumlah_koresponden) ? $jumlah_koresponden:  "0" . " Orang";?>
                            </h3>
                            <p>Responden</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                	</div>
                  <div class="col-lg-4 col-xs-6">
                    
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                              <?php echo isset($jumlah_koresponden) ? $jumlah_aspek_skm:  "0" . " Aspek";?>
                            </h3>
                            <p>Penilaian</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-check-square-o"></i>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <!-- Diagram -->

                  <div class="row">
                       <div class="col-lg-8 col-xs-12">
                          <div id="graft" style="height: 400px; margin: 0 auto"></div>
                      </div>
                      <div class="col-lg-4 col-xs-12">
                          <div id="container" style="height: 400px; margin: 0 auto"></div>
                      </div>
                  </div>
                 

                  <!-- End Diagram -->

                </div>
                 <div class="row">
                  <!-- Diagram -->

                  <div class="row">
                       <div class="col-lg-8 col-xs-12">
                          <!-- <table class="table table-striped">
                            <tr>
                                <th>Nilai</th>
                                <th>Jumlah</th>
                            </tr>
                            <?php foreach($tabel_grafik_pie as $dgp){ ?>
                                <tr>
                                    <td><?php echo $dgp['nilai']; ?></td>                                    
                                    <td><?php echo $dgp['jumlah']; ?></td>
                                </tr>
                            <?php } ?>
                          </table> -->
                      </div>
                      <div class="col-lg-4 col-xs-12">
                          <table class="table table-striped">
                            <tr>
                                <th>Nilai</th>
                                <th>Jumlah</th>
                            </tr>
                            <?php foreach($tabel_grafik_pie as $dgp){ ?>
                                <tr>
                                    <td><?php echo $dgp['nilai']; ?></td>                                    
                                    <td>
                                        <ul>
                                            <?php foreach($dgp['result'] as $re){ ?>
                                                <li><?php echo $re['nama']; ?> : <?php echo $re['jumlah']; ?></li>
                                            <?php } ?>
                                        </ul>
                                        <!-- <?php echo $dgp['jumlah']; ?> -->
                                    </td>
                                </tr>
                            <?php } ?>
                          </table>
                      </div>
                  </div>
                 

                  <!-- End Diagram -->

                </div>

      
            </div>
        </div>
    </div>
</div>

<body>

    <script type="text/javascript">
      // Build the chart
  
      Highcharts.chart('graft', {
          chart: {
              type: 'column'
          },
          title: {
              text: 'Grafik Kepuasan Tamu'
          },
          subtitle: {
              text: 'Dalam 5 Bulan Terakhir'
          },
          xAxis: {
              categories: <?php echo json_encode($sumbux_grafik); ?>,
              crosshair: true
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Banyak Penilai (orang)'
              }
          },
          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y:.1f} orang</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
          series: <?php echo json_encode($data_grafik); ?>
      });

       Highcharts.chart('container', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          title: {
              text: 'Grafik Kepuasan Tamu'
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: false
                  },
                  showInLegend: true
              }
          },
          series: [{
              name: 'Persentase',
              colorByPoint: true,
              data: <?php echo json_encode($data_grafik_pie); ?>
          }]
      });

    </script>
    <!-- js untuk jquery -->
  <script src="js/jquery-1.11.2.min.js"></script>
  <!-- js untuk bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- js untuk moment -->
  <script src="js/moment.js"></script>
  <!-- js untuk bootstrap datetimepicker -->
  <script src="js/bootstrap-datetimepicker.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
         $("#btn_cari").submit();
       $('#tanggal').datetimepicker({
        format : 'DD/MM/YYYY'
       });
    });
  </script>
</body>