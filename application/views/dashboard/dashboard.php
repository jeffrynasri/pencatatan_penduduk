<div class="box-body">
    <div class="col-md-12">
        <div class="box" style="padding: 15px">
            <div class="box-header">
                <h3 class="box-title">Dashboard Penduduk</h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                  <?php echo form_open('dashboard') ?>  
                    <div class="row" style="margin-bottom: 6px;">
                      <div class="col-sm-4">    
                        <!-- <label class='control-label' for='id_kecamatan'>Tempat</label>    -->
                        <div class="form-group">
                            <select class="form-control mb-1" name="id_kecamatan" id='id_kecamatan'>
                              <option value="-1">Semua</option>
                              <?php foreach($all_kecamatan as $kecamatan){ ?>
                                <option value="<?php echo $kecamatan['id']; ?>" <?php echo ($this->input->post('id_kecamatan') && $this->input->post('id_kecamatan')==$kecamatan['id'])?'selected':''; ?> ><?php echo $kecamatan['nama']; ?></option>
                              <?php } ?>
                            </select>
                            <select class="form-control mb-1" name="id_desa" id='id_desa'>
                              <option value="-1">Semua</option>
                              <?php foreach($all_desa as $desa){ ?>
                                <option value="<?php echo $desa['id']; ?>" <?php echo ($this->input->post('id_desa') && $this->input->post('id_desa')==$desa['id'])?'selected':''; ?> ><?php echo $desa['nama']; ?></option>
                              <?php } ?>
                            </select>
                        </div>
                      </div> 
                      <div class='col-sm-2'>
                        <button class='btn btn-primary' type="submit">Pilih</button>
                      </div>  
                  <?php echo form_close(); ?>
                </div>
                <div class="col-lg-12 col-xs-12">
                  <div id="graft_pendidikan" style="height: 400px; margin: 0 auto"></div>
                </div>
                <table class='table table-hover'>
                  <tbody>
                    <tr>
                      <th>Nama</th>
                      <th>Jumlah</th>
                    </tr>
                    <?php foreach($data_grafik_pendidikan as $grafik_pendidikan){ ?>
                      <tr>
                        <td><?php echo $grafik_pendidikan['name']; ?></td>
                        <td><?php echo $grafik_pendidikan['y']; ?></td>
                      </tr
                    <?php } ?>
                  </tbody>
                </table>
                <div class="col-lg-12 col-xs-12">
                  <div id="graft_pekerjaan" style="height: 400px; margin: 0 auto"></div>
                </div>
                <table class='table table-hover'>
                  <tbody>
                    <tr>
                      <th>Nama</th>
                      <th>Jumlah</th>
                    </tr>
                    <?php foreach($data_grafik_pekerjaan as $grafik_pekerjaan){ ?>
                      <tr>
                        <td><?php echo $grafik_pekerjaan['name']; ?></td>
                        <td><?php echo $grafik_pekerjaan['y']; ?></td>
                      </tr
                    <?php } ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<body>
    <script type="text/javascript">
        function draw_grafik(){
            //Grafik Pendidikan
            Highcharts.chart('graft_pendidikan', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Grafik Pendidikan'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Pendidikan',
                colorByPoint: true,
                data: <?php echo json_encode($data_grafik_pendidikan); ?>
            }]
          });
          //Grafik Pekerjaan
          Highcharts.chart('graft_pekerjaan', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Grafik Pekerjaan'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Pekerjaan',
                colorByPoint: true,
                data: <?php echo json_encode($data_grafik_pekerjaan); ?>
            }]
          });
        }

        draw_grafik();
        $('#id_kecamatan').on('change',function(){
          console.log("jos")
          $.ajax({
            url: '<?php echo site_url('dashboard/get_desa_by_idkecamatan_json'); ?>',
            type: 'POST',
            dataType: 'json',
            data:{
              'id_kecamatan':$('#id_kecamatan option:selected').val()
            },
            success: function(data){
              console.log(data)
              $('#id_desa').empty()
              $('#id_desa').append('<option value="-1">Semua</option>')
              for(var desa of data){
                $('#id_desa').append("<option value='"+desa.id+"'>"+desa.nama+"</option>")
              }
            }
          })
        })
        
      </script>

    <!-- js untuk jquery -->
  <script src="js/jquery-1.11.2.min.js"></script>
  <!-- js untuk bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- js untuk moment -->
  <script src="js/moment.js"></script>
  <!-- js untuk bootstrap datetimepicker -->
  <script src="js/bootstrap-datetimepicker.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
         $("#btn_cari").submit();
       $('#tanggal').datetimepicker({
        format : 'DD/MM/YYYY'
       });
    });
  </script>
</body>