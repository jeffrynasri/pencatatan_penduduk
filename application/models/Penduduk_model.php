<?php

class Penduduk_model extends CI_Model
{
  // id
  // nik
  // no_kk
  // hub_dalam_kk
  // nama
  // tempat_lahir
  // tanggal_lahir
  // agama
  // is_laki
  // pendidikan_dalamkk
  // pekerjaan
  // status_warga_negara
  // nik_ayah
  // nama_ayah
  // nik_ibu
  // nama_ibu
  // id_kecamatan
  // id_desa
  // alamat
  // rt
  // rw
  // no_telepon
  // alamat_email
  // status_perkawinan
  // no_akta_nikah
  // golongan_darah
  // gambar_profil
  // gambar_ktp
  // gambar_kk
  // updated_at
  // created_at
  // keterangan
  // updated by
  // created by
  // status_bantuan

    var $table = 'penduduk';
    var $column_order = array('penduduk.nik','penduduk.no_kk','penduduk.hub_dalam_kk','penduduk.nama','penduduk.tempat_lahir','penduduk.tanggal_lahir','penduduk.agama','penduduk.is_laki','penduduk.pendidikan_dalamkk','penduduk.pekerjaan','penduduk.status_warga_negara','penduduk.nik_ayah','penduduk.nama_ayah','penduduk.nik_ibu','penduduk.nama_ibu','penduduk.id_kecamatan','penduduk.nama_id_desa','penduduk.alamat','penduduk.rt','penduduk.rw','penduduk.no_telepon','penduduk.alamat_email','penduduk.status_perkawinan','penduduk.no_akta_nikah','penduduk.golongan_darah','penduduk.gambar_profil','penduduk.gambar_ktp','penduduk.gambar_kk','penduduk.keterangan','penduduk.status_bantuan'); 
    var $column_search = array('penduduk.nik','penduduk.no_kk','penduduk.hub_dalam_kk','penduduk.nama','penduduk.tempat_lahir','penduduk.tanggal_lahir','penduduk.agama','penduduk.is_laki','penduduk.pendidikan_dalamkk','penduduk.pekerjaan','penduduk.status_warga_negara','penduduk.nik_ayah','penduduk.nama_ayah','penduduk.nik_ibu','penduduk.nama_ibu','penduduk.id_kecamatan','penduduk.nama_id_desa','penduduk.alamat','penduduk.rt','penduduk.rw','penduduk.no_telepon','penduduk.alamat_email','penduduk.status_perkawinan','penduduk.no_akta_nikah','penduduk.golongan_darah','penduduk.gambar_profil','penduduk.gambar_ktp','penduduk.gambar_kk','penduduk.keterangan','penduduk.status_bantuan');
    var $order = array('penduduk.nama' => 'asc'); // default order


    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='penduduk.*',$order_by="nama",$group_by='-1')
    {
      $this->db->where($params);
      $this->db->order_by($order_by, 'asc');
      $this->db->select($select.',tweb_desa.nama as nama_desa,tweb_golongan_darah.nama as nama_goldar,tweb_kecamatan.nama as nama_kecamatan,tweb_penduduk_agama.nama as nama_agama,tweb_penduduk_hubungan.nama as nama_hubungan,tweb_penduduk_kawin.nama as nama_kawin,tweb_penduduk_pekerjaan.nama as nama_pekerjaan,tweb_penduduk_pendidikan_kk.nama as nama_pendidikan,tweb_penduduk_warganegara.nama as nama_warganegara,tweb_status_bantuan.nama as nama_status_bantuan');
      $this->db->join('tweb_penduduk_hubungan','penduduk.hub_dalam_kk = tweb_penduduk_hubungan.id');
      $this->db->join('tweb_penduduk_agama','penduduk.agama = tweb_penduduk_agama.id');
      $this->db->join('tweb_penduduk_pendidikan_kk','penduduk.pendidikan_dalamkk = tweb_penduduk_pendidikan_kk.id');
      $this->db->join('tweb_penduduk_pekerjaan','penduduk.pekerjaan = tweb_penduduk_pekerjaan.id');
      $this->db->join('tweb_penduduk_warganegara','penduduk.status_warga_negara = tweb_penduduk_warganegara.id');
      $this->db->join('tweb_penduduk_kawin','penduduk.status_perkawinan = tweb_penduduk_kawin.id');
      $this->db->join('tweb_golongan_darah','penduduk.golongan_darah = tweb_golongan_darah.id');
      $this->db->join('tweb_status_bantuan','penduduk.status_bantuan = tweb_status_bantuan.id');
      $this->db->join('tweb_desa','penduduk.id_desa = tweb_desa.id');
      $this->db->join('tweb_kecamatan','penduduk.id_kecamatan = tweb_kecamatan.id');
      if($group_by != '-1'){
        $this->db->group_by($group_by);
      }
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
    }  
    //UNTUK DATATABEL GROUP BY
    private function _get_datatables_querygroupby($params=array(),$select='penduduk.*',$order_by="nama",$group_by)
    {
      $this->db->where($params);
      $this->db->select($select);
      $this->db->group_by($group_by);
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
  
      if(isset($_POST['order']))
      {
        //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        //$this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='penduduk.*',$order_by="nama",$group_by='-1')
    {
      if($group_by!='-1'){
        $this->_get_datatables_querygroupby( $params?$params:array(),$select?$select:'penduduk.*',$order_by?$order_by:'nama',($group_by!='-1')?$group_by:'-1' );
      }else{
        $this->_get_datatables_query( $params?$params:array(),$select?$select:'penduduk.*',$order_by?$order_by:'nama',($group_by!='-1')?$group_by:'-1' );
      }
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='penduduk.*',$order_by="nama",$group_by='-1')
    {
      if($group_by!='-1'){
        $this->_get_datatables_querygroupby( $params?$params:array(),$select?$select:'penduduk.*',$order_by?$order_by:'nama',($group_by!='-1')?$group_by:'-1' );
      }else{
        $this->_get_datatables_query( $params?$params:array(),$select?$select:'penduduk.*',$order_by?$order_by:'nama',($group_by!='-1')?$group_by:'-1' );
      }
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='penduduk.*',$order_by="nama",$group_by='-1')
    {
      $this->db->select($select);
      $this->db->from($this->table);
      $this->db->where($params);
      if($group_by!='-1'){
        $this->db->group_by($group_by);
      }
      return $this->db->count_all_results();
    }
    
    /*
     * Get penduduk by id
     */
    function get_penduduk($array_where)
    {
        $this->db->where($array_where);
        $this->db->select('penduduk.*,tweb_desa.nama as nama_desa,tweb_golongan_darah.nama as nama_goldar,tweb_kecamatan.nama as nama_kecamatan,tweb_penduduk_agama.nama as nama_agama,tweb_penduduk_hubungan.nama as nama_hubungan,tweb_penduduk_kawin.nama as nama_kawin,tweb_penduduk_pekerjaan.nama as nama_pekerjaan,tweb_penduduk_pendidikan_kk.nama as nama_pendidikan,tweb_penduduk_warganegara.nama as nama_warganegara,tweb_status_bantuan.nama as nama_status_bantuan');
        $this->db->join('tweb_penduduk_hubungan','penduduk.hub_dalam_kk = tweb_penduduk_hubungan.id');
        $this->db->join('tweb_penduduk_agama','penduduk.agama = tweb_penduduk_agama.id');
        $this->db->join('tweb_penduduk_pendidikan_kk','penduduk.pendidikan_dalamkk = tweb_penduduk_pendidikan_kk.id');
        $this->db->join('tweb_penduduk_pekerjaan','penduduk.pekerjaan = tweb_penduduk_pekerjaan.id');
        $this->db->join('tweb_penduduk_warganegara','penduduk.status_warga_negara = tweb_penduduk_warganegara.id');
        $this->db->join('tweb_penduduk_kawin','penduduk.status_perkawinan = tweb_penduduk_kawin.id');
        $this->db->join('tweb_golongan_darah','penduduk.golongan_darah = tweb_golongan_darah.id');
        $this->db->join('tweb_status_bantuan','penduduk.status_bantuan = tweb_status_bantuan.id');
        $this->db->join('tweb_desa','penduduk.id_desa = tweb_desa.id');
        $this->db->join('tweb_kecamatan','penduduk.id_kecamatan = tweb_kecamatan.id');
        return $this->db->get('penduduk')->row_array();
    }
  
    
   /*
     * Get all penduduk
     */
    function get_all_penduduk($select='penduduk.*',$params_wehere=array())
    {
      $this->db->select($select.',tweb_desa.nama as nama_desa,tweb_golongan_darah.nama as nama_goldar,tweb_kecamatan.nama as nama_kecamatan,tweb_penduduk_agama.nama as nama_agama,tweb_penduduk_hubungan.nama as nama_hubungan,tweb_penduduk_kawin.nama as nama_kawin,tweb_penduduk_pekerjaan.nama as nama_pekerjaan,tweb_penduduk_pendidikan_kk.nama as nama_pendidikan,tweb_penduduk_warganegara.nama as nama_warganegara,tweb_status_bantuan.nama as nama_status_bantuan');
      $this->db->join('tweb_penduduk_hubungan','penduduk.hub_dalam_kk = tweb_penduduk_hubungan.id');
      $this->db->join('tweb_penduduk_agama','penduduk.agama = tweb_penduduk_agama.id');
      $this->db->join('tweb_penduduk_pendidikan_kk','penduduk.pendidikan_dalamkk = tweb_penduduk_pendidikan_kk.id');
      $this->db->join('tweb_penduduk_pekerjaan','penduduk.pekerjaan = tweb_penduduk_pekerjaan.id');
      $this->db->join('tweb_penduduk_warganegara','penduduk.status_warga_negara = tweb_penduduk_warganegara.id');
      $this->db->join('tweb_penduduk_kawin','penduduk.status_perkawinan = tweb_penduduk_kawin.id');
      $this->db->join('tweb_golongan_darah','penduduk.golongan_darah = tweb_golongan_darah.id');
      $this->db->join('tweb_status_bantuan','penduduk.status_bantuan = tweb_status_bantuan.id');
      $this->db->join('tweb_desa','penduduk.id_desa = tweb_desa.id');
      $this->db->join('tweb_kecamatan','penduduk.id_kecamatan = tweb_kecamatan.id');
      $this->db->where($params_wehere);
      return $this->db->get('penduduk')->result_array();
    }

    /*
     * Get all penduduk with group by
     */
    function get_all_penduduk_groupby($select='penduduk.*',$params_wehere=array(),$group_by='')
    {
        $this->db->select($select);
        $this->db->where($params_wehere);
        $this->db->group_by($group_by);
        return $this->db->get('penduduk')->result_array();
    }
        
    /*
     * function to add new penduduk
     */
    function add_penduduk($params)
    {
        return $this->db->insert('penduduk',$params);
    }
    
    /*
     * function to update penduduk
     */
    function update_penduduk($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('penduduk',$params);
    }
    
    /*
     * function to delete penduduk
     */
    function delete_penduduk($id)
    {
        return $this->db->delete('penduduk',array('id'=>$id));
    }
}
