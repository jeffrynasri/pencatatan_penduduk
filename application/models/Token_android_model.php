<?php

class Token_android_model extends CI_Model
{
    //id	
    //token	
    //user_id
    //created_at
    //expired_at
    var $table = 'token_android';
    var $column_order = array('token_android.token'); 
    var $column_search = array('token_android.token');
    var $order = array('token_android.nama' => 'asc'); // default order


    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='token_android.*',$order_by="token")
    {
      $this->db->where($params);
      $this->db->order_by($order_by, 'asc');
      $this->db->select($select);
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
      
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='token_android.*',$order_by="nama")
    {
      $this->_get_datatables_query( $params?$params:array(),$select?$select:'token_android.*',$order_by?$order_by:'nama' );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='token_android.*',$order_by="nama")
    {
      $this->_get_datatables_query($params?$params:array(),$select?$select:'token_android.*',$order_by?$order_by:'nama');
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='token_android.*',$order_by="nama")
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get token_android by id
     */
    function get_token_android($array)
    {
        return $this->db->get_where('token_android',$array)->row_array();
    }
  
    
   /*
     * Get all token_android
     */
    function get_all_token_android($params=array())
    {
        $this->db->order_by('token', 'asc');
        $this->db->where($params);
        return $this->db->get('token_android')->result_array();
    }
        
    /*
     * function to add new token_android
     */
    function add_token_android($params)
    {
        return $this->db->insert('token_android',$params);
    }
    
    /*
     * function to update token_android
     */
    function update_token_android($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('token_android',$params);
    }
    
    /*
     * function to delete token_android
     */
    function delete_token_android($array)
    {
        return $this->db->delete('token_android',$array);
    }
}
