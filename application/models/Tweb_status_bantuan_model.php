<?php

class Tweb_status_bantuan_model extends CI_Model
{
    // id
    //nama

    var $table = 'tweb_status_bantuan';
    var $column_order = array('tweb_status_bantuan.nama'); 
    var $column_search = array('tweb_status_bantuan.nama');
    var $order = array('tweb_status_bantuan.nama' => 'asc'); // default order


    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='tweb_status_bantuan.*',$order_by="nama")
    {
      $this->db->where($params);
      $this->db->order_by($order_by, 'asc');
      $this->db->select($select);
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
      
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='tweb_status_bantuan.*',$order_by="nama")
    {
      $this->_get_datatables_query( $params?$params:array(),$select?$select:'tweb_status_bantuan.*',$order_by?$order_by:'nama' );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='tweb_status_bantuan.*',$order_by="nama")
    {
      $this->_get_datatables_query($params?$params:array(),$select?$select:'tweb_status_bantuan.*',$order_by?$order_by:'nama');
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='tweb_status_bantuan.*',$order_by="nama")
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get tweb_status_bantuan by id
     */
    function get_tweb_status_bantuan($id)
    {
        return $this->db->get_where('tweb_status_bantuan',array('tweb_status_bantuan.id'=>$id))->row_array();
    }
  
    
   /*
     * Get all tweb_status_bantuan
     */
    function get_all_tweb_status_bantuan($params=array())
    {
        $this->db->order_by('nama', 'asc');
        $this->db->where($params);
        return $this->db->get('tweb_status_bantuan')->result_array();
    }
        
    /*
     * function to add new tweb_status_bantuan
     */
    function add_tweb_status_bantuan($params)
    {
        return $this->db->insert('tweb_status_bantuan',$params);
    }
    
    /*
     * function to update tweb_status_bantuan
     */
    function update_tweb_status_bantuan($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('tweb_status_bantuan',$params);
    }
    
    /*
     * function to delete tweb_status_bantuan
     */
    function delete_tweb_status_bantuan($id)
    {
        return $this->db->delete('tweb_status_bantuan',array('id'=>$id));
    }
}
